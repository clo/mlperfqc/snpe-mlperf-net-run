# Copyright (c) 2019 Qualcomm Innovation Center, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name Qualcomm Innovation Center nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

TOPDIR := $(CURDIR)
PATH := ${TOPDIR}/external/cmake-3.15.1-Linux-x86_64/bin:${PATH}

all: build

.PHONY build: loadgen_build/libmlperf_loadgen.a external/status/all.stamp \
		jni/mlperf_snpe/snpe_python/coco_snpe.py \
		jni/mlperf_snpe/snpe_python/imagenet_snpe.py \
		jni/mlperf_snpe/snpe_python/backend_snpe.py \
		jni/mlperf_snpe/snpe_python/main_snpe.py
	@[ ! -d "${SNPE_ROOT}" ] && (echo "ERROR: SNPE_ROOT not set, aborting." && false) || true
	./external/android-ndk-r17c/ndk-build

external/status/all.stamp:
	cd external && make
	touch $@
	
jni/mlperf_snpe/snpe_python/coco_snpe.py: external/status/all.stamp patch/coco_snpe.py.patch
	cp ./external/mlperf_inference/v0.5/classification_and_detection/python/coco.py jni/mlperf_snpe/snpe_python/coco_snpe.py
	patch -p1 < patch/coco_snpe.py.patch

jni/mlperf_snpe/snpe_python/imagenet_snpe.py: external/status/all.stamp patch/imagenet_snpe.py.patch
	cp ./external/mlperf_inference/v0.5/classification_and_detection/python/imagenet.py jni/mlperf_snpe/snpe_python/imagenet_snpe.py
	patch -p1 < patch/imagenet_snpe.py.patch

jni/mlperf_snpe/snpe_python/backend_snpe.py: external/status/all.stamp patch/backend_snpe.py.patch
	cp ./external/mlperf_inference/v0.5/classification_and_detection/python/backend.py jni/mlperf_snpe/snpe_python/backend_snpe.py
	patch -p1 < patch/backend_snpe.py.patch

jni/mlperf_snpe/snpe_python/main_snpe.py: external/status/all.stamp patch/main_snpe.py.patch
	cp ./external/mlperf_inference/v0.5/classification_and_detection/python/main.py jni/mlperf_snpe/snpe_python/main_snpe.py
	patch -p1 < patch/main_snpe.py.patch

loadgen_build/libmlperf_loadgen.a: external/status/all.stamp
	mkdir -p loadgen_build
	cd loadgen_build && \
	cmake ../external/mlperf_inference/loadgen \
                -DCMAKE_SYSTEM_NAME=Android \
		-DCMAKE_SYSTEM_VERSION=21 \
		-DCMAKE_ANDROID_ARCH_ABI=armeabi-v7a \
		-DCMAKE_ANDROID_NDK=${TOPDIR}/external/android-ndk-r17c \
		-DCMAKE_ANDROID_STL_TYPE=c++_shared \
		-DCMAKE_ANDROID_NDK_TOOLCHAIN_VERSION=clang \
		-DCMAKE_CXX_FLAGS=-std=c++14
	cd loadgen_build && make

clean:
	rm -rf loadgen_build
	rm -rf libs obj
	rm -f jni/mlperf_snpe/snpe_python/coco_snpe.py
	rm -f jni/mlperf_snpe/snpe_python/imagenet_snpe.py
	rm -f jni/mlperf_snpe/snpe_python/backend_snpe.py
	rm -f jni/mlperf_snpe/snpe_python/main_snpe.py
	rm -rf newpatch/a newpatch/b newpatch/patch
	cd external && make clean
