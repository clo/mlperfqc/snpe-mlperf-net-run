--- a/jni/mlperf_snpe/snpe_python/main_snpe.py	2019-10-15 11:27:14.124976860 -0700
+++ b/jni/mlperf_snpe/snpe_python/main_snpe.py	2019-10-15 11:27:14.128976942 -0700
@@ -1,3 +1,36 @@
+# Changes from QuIC are made under the following license
+# Copyright (c) 2019 Qualcomm Innovation Center, Inc.
+#
+# Redistribution and use in source and binary forms, with or without
+# modification, are permitted (subject to the limitations in the
+# disclaimer below) provided that the following conditions are met:
+#
+#    * Redistributions of source code must retain the above copyright
+#      notice, this list of conditions and the following disclaimer.
+#
+#    * Redistributions in binary form must reproduce the above
+#      copyright notice, this list of conditions and the following
+#      disclaimer in the documentation and/or other materials provided
+#      with the distribution.
+#
+#    * Neither the name Qualcomm Innovation Center nor the names of its
+#      contributors may be used to endorse or promote products derived
+#      from this software without specific prior written permission.
+#
+# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
+# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
+# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
+# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
+# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
+# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
+# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
+# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
+# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
+# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
+# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
+# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+
 """
 mlperf inference benchmarking tool
 """
@@ -22,7 +55,11 @@
 
 import dataset
 import imagenet
+import imagenet_snpe
+import coco_snpe
 import coco
+from backend_snpe import BackendSNPE
+from main import *
 
 logging.basicConfig(level=logging.INFO)
 log = logging.getLogger("main")
@@ -33,138 +70,57 @@
 # pylint: disable=missing-docstring
 
 # the datasets we support
-SUPPORTED_DATASETS = {
-    "imagenet":
-        (imagenet.Imagenet, dataset.pre_process_vgg, dataset.PostProcessCommon(offset=-1),
+SUPPORTED_DATASETS.update({
+    "imagenet_mobilenet_snpe":
+        (imagenet_snpe.ImagenetSNPE, dataset.pre_process_mobilenet, dataset.PostProcessArgMax(offset=-1),
          {"image_size": [224, 224, 3]}),
-    "imagenet_mobilenet":
-        (imagenet.Imagenet, dataset.pre_process_mobilenet, dataset.PostProcessArgMax(offset=-1),
+    "imagenet_resnet50_snpe":
+        (imagenet_snpe.ImagenetSNPE, dataset.pre_process_vgg, dataset.PostProcessArgMax(offset=-1),    
          {"image_size": [224, 224, 3]}),
-    "coco-300":
-        (coco.Coco, dataset.pre_process_coco_mobilenet, coco.PostProcessCoco(),
+    "coco-300_snpe":
+        (coco_snpe.CocoSNPE, coco_snpe.pre_process_coco_mobilenet, coco.PostProcessCoco(),
          {"image_size": [300, 300, 3]}),
-    "coco-300-pt":
-        (coco.Coco, dataset.pre_process_coco_pt_mobilenet, coco.PostProcessCocoPt(False,0.3),
-         {"image_size": [300, 300, 3]}),         
-    "coco-1200":
-        (coco.Coco, dataset.pre_process_coco_resnet34, coco.PostProcessCoco(),
-         {"image_size": [1200, 1200, 3]}),
-    "coco-1200-onnx":
-        (coco.Coco, dataset.pre_process_coco_resnet34, coco.PostProcessCocoOnnx(),
-         {"image_size": [1200, 1200, 3]}),
-    "coco-1200-pt":
-        (coco.Coco, dataset.pre_process_coco_resnet34, coco.PostProcessCocoPt(True,0.05),
-         {"image_size": [1200, 1200, 3],"use_label_map": True}),
-    "coco-1200-tf":
-        (coco.Coco, dataset.pre_process_coco_resnet34, coco.PostProcessCocoTf(),
-         {"image_size": [1200, 1200, 3],"use_label_map": False}),
-}
-
-# pre-defined command line options so simplify things. They are used as defaults and can be
-# overwritten from command line
-
-SUPPORTED_PROFILES = {
-    "defaults": {
-        "dataset": "imagenet",
-        "backend": "tensorflow",
-        "cache": 0,
-        "max-batchsize": 32,
-    },
+})
 
-    # resnet
-    "resnet50-tf": {
-        "inputs": "input_tensor:0",
-        "outputs": "ArgMax:0",
-        "dataset": "imagenet",
-        "backend": "tensorflow",
-        "model-name": "resnet50",
-    },
-    "resnet50-onnxruntime": {
-        "dataset": "imagenet",
-        "outputs": "ArgMax:0",
-        "backend": "onnxruntime",
-        "model-name": "resnet50",
+SUPPORTED_PROFILES.update({
+    # resnet50-snpe
+    "resnet50-snpe": {
+        "dataset": "imagenet_resnet50_snpe",
+        "inputs": "input_tensor",
+        "outputs": "softmax_tensor:0",
+        "backend": "snpe",
+        "model_type": "classifier"
     },
 
-    # mobilenet
-    "mobilenet-tf": {
+    # mobilenet-snpe
+    "mobilenet-snpe": {
+        "dataset": "imagenet_mobilenet_snpe",
         "inputs": "input:0",
         "outputs": "MobilenetV1/Predictions/Reshape_1:0",
-        "dataset": "imagenet_mobilenet",
-        "backend": "tensorflow",
-        "model-name": "mobilenet",
-    },
-    "mobilenet-onnxruntime": {
-        "dataset": "imagenet_mobilenet",
-        "outputs": "MobilenetV1/Predictions/Reshape_1:0",
-        "backend": "onnxruntime",
-        "model-name": "mobilenet",
+        "backend": "snpe",
+        "model_type": "classifier"
     },
 
-    # ssd-mobilenet
-    "ssd-mobilenet-tf": {
-        "inputs": "image_tensor:0",
+    # ssd-mobilenet-snpe
+    "ssd-mobilenet-snpe": {
+        "dataset": "coco-300_snpe",
+        "inputs": "Preprocessor/sub",
         "outputs": "num_detections:0,detection_boxes:0,detection_scores:0,detection_classes:0",
-        "dataset": "coco-300",
-        "backend": "tensorflow",
-        "model-name": "ssd-mobilenet",
-    },
-    "ssd-mobilenet-pytorch": {
-        "inputs": "image",
-        "outputs": "bboxes,labels,scores",
-        "dataset": "coco-300-pt",
-        "backend": "pytorch-native",
-        "model-name": "ssd-mobilenet",
-    },
-    "ssd-mobilenet-onnxruntime": {
-        "dataset": "coco-300",
-        "outputs": "num_detections:0,detection_boxes:0,detection_scores:0,detection_classes:0",
-        "backend": "onnxruntime",
+        "backend": "snpe",
         "data-format": "NHWC",
-        "model-name": "ssd-mobilenet",
+        "model_type": "objectdetection"
     },
 
-    # ssd-resnet34
-    "ssd-resnet34-tf": {
-        "inputs": "image:0",
-        "outputs": "detection_bboxes:0,detection_classes:0,detection_scores:0",
-        "dataset": "coco-1200-tf",
-        "backend": "tensorflow",
-        "data-format": "NCHW",
-        "model-name": "ssd-resnet34",
-    },
-    "ssd-resnet34-pytorch": {
-        "inputs": "image",
-        "outputs": "bboxes,labels,scores",
-        "dataset": "coco-1200-pt",
-        "backend": "pytorch-native",
-        "model-name": "ssd-resnet34",
-    },
-    "ssd-resnet34-onnxruntime": {
-        "dataset": "coco-1200-onnx",
+    # ssd-resnet34-snpe
+    "ssd-resnet34-snpe": {
+        "dataset": "coco-1200-snpe",
         "inputs": "image",
         "outputs": "bboxes,labels,scores",
-        "backend": "onnxruntime",
+        "backend": "snpe",
         "data-format": "NCHW",
-        "max-batchsize": 1,
-        "model-name": "ssd-resnet34",
-    },
-    "ssd-resnet34-onnxruntime-tf": {
-        "dataset": "coco-1200-tf",
-        "inputs": "image:0",
-        "outputs": "detection_bboxes:0,detection_classes:0,detection_scores:0",
-        "backend": "onnxruntime",
-        "data-format": "NHWC",
-        "model-name": "ssd-resnet34",
+        "model_type": "objectdetection"
     },
-}
-
-SCENARIO_MAP = {
-    "SingleStream": lg.TestScenario.SingleStream,
-    "MultiStream": lg.TestScenario.MultiStream,
-    "Server": lg.TestScenario.Server,
-    "Offline": lg.TestScenario.Offline,
-}
+})
 
 last_timeing = []
 
@@ -200,6 +156,10 @@
     parser.add_argument("--count", type=int, help="dataset items to use")
     parser.add_argument("--max-latency", type=float, help="mlperf max latency in pct tile")
     parser.add_argument("--samples-per-query", type=int, help="mlperf multi-stream sample per query")
+    # SNPE arguments
+    parser.add_argument("--model_type", default="classifier", help="model type classifier|objectdetection")
+    parser.add_argument("--runtime", default="cpu",
+                        help="Select runtime cpu|gpu|aip|dsp on which to run inference")
     args = parser.parse_args()
 
     # don't use defaults in argparser. Instead we default to a dict, override that with a profile
@@ -220,147 +180,22 @@
 
     if args.scenario not in SCENARIO_MAP:
         parser.error("valid scanarios:" + str(list(SCENARIO_MAP.keys())))
+    if args.find_peak_performance and args.scenario == "SingleStream":
+        parser.error("Finding peak performance is only supported in MultiStream,"
+                     " MultiStreamFree, and Server scenarios.")
     return args
 
 
-def get_backend(backend):
-    if backend == "tensorflow":
-        from backend_tf import BackendTensorflow
-        backend = BackendTensorflow()
-    elif backend == "onnxruntime":
-        from backend_onnxruntime import BackendOnnxruntime
-        backend = BackendOnnxruntime()
-    elif backend == "null":
-        from backend_null import BackendNull
-        backend = BackendNull()
-    elif backend == "pytorch":
-        from backend_pytorch import BackendPytorch
-        backend = BackendPytorch()
-    elif backend == "pytorch-native":
-        from backend_pytorch_native import BackendPytorchNative
-        backend = BackendPytorchNative()      
-    elif backend == "tflite":
-        from backend_tflite import BackendTflite
-        backend = BackendTflite()
-    else:
-        raise ValueError("unknown backend: " + backend)
-    return backend
-
-
-class Item:
-    """An item that we queue for processing by the thread pool."""
-
-    def __init__(self, query_id, content_id, img, label=None):
-        self.query_id = query_id
-        self.content_id = content_id
-        self.img = img
-        self.label = label
-        self.start = time.time()
-
-
-class RunnerBase:
-    def __init__(self, model, ds, threads, post_proc=None, max_batchsize=128):
-        self.take_accuracy = False
-        self.ds = ds
-        self.model = model
-        self.post_process = post_proc
-        self.threads = threads
-        self.take_accuracy = False
-        self.max_batchsize = max_batchsize
-        self.result_timing = []
+def get_backend_snpe(backend, args):
+    if backend == "snpe":
+        from backend_snpe import BackendSNPE
+        backend = BackendSNPE(args.accuracy, args.scenario, args.model_type,
+                              args.runtime, os.path.abspath(args.config),
+                              args.find_peak_performance, args.samples_per_query,
+                              args.model_name, args.qps, args.max_latency * 1000)
+        return backend
 
-    def handle_tasks(self, tasks_queue):
-        pass
-
-    def start_run(self, result_dict, take_accuracy):
-        self.result_dict = result_dict
-        self.result_timing = []
-        self.take_accuracy = take_accuracy
-        self.post_process.start()
-
-    def run_one_item(self, qitem):
-        # run the prediction
-        processed_results = []
-        try:
-            results = self.model.predict({self.model.inputs[0]: qitem.img})
-            processed_results = self.post_process(results, qitem.content_id, qitem.label, self.result_dict)
-            if self.take_accuracy:
-                self.post_process.add_results(processed_results)
-                self.result_timing.append(time.time() - qitem.start)
-        except Exception as ex:  # pylint: disable=broad-except
-            src = [self.ds.get_item_loc(i) for i in qitem.content_id]
-            log.error("thread: failed on contentid=%s, %s", src, ex)
-            # since post_process will not run, fake empty responses
-            processed_results = [[]] * len(qitem.query_id)
-        finally:
-            response_array_refs = []
-            response = []
-            for idx, query_id in enumerate(qitem.query_id):
-                response_array = array.array("B", np.array(processed_results[idx], np.float32).tobytes())
-                response_array_refs.append(response_array)
-                bi = response_array.buffer_info()
-                response.append(lg.QuerySampleResponse(query_id, bi[0], bi[1]))
-            lg.QuerySamplesComplete(response)
-
-    def enqueue(self, query_samples):
-        idx = [q.index for q in query_samples]
-        query_id = [q.id for q in query_samples]
-        if len(query_samples) < self.max_batchsize:
-            data, label = self.ds.get_samples(idx)
-            self.run_one_item(Item(query_id, idx, data, label))
-        else:
-            bs = self.max_batchsize
-            for i in range(0, len(idx), bs):
-                data, label = self.ds.get_samples(idx[i:i+bs])
-                self.run_one_item(Item(query_id[i:i+bs], idx[i:i+bs], data, label))
-
-    def finish(self):
-        pass
-
-
-class QueueRunner(RunnerBase):
-    def __init__(self, model, ds, threads, post_proc=None, max_batchsize=128):
-        super().__init__(model, ds, threads, post_proc, max_batchsize)
-        self.tasks = Queue(maxsize=threads * 4)
-        self.workers = []
-        self.result_dict = {}
-
-        for _ in range(self.threads):
-            worker = threading.Thread(target=self.handle_tasks, args=(self.tasks,))
-            worker.daemon = True
-            self.workers.append(worker)
-            worker.start()
-
-    def handle_tasks(self, tasks_queue):
-        """Worker thread."""
-        while True:
-            qitem = tasks_queue.get()
-            if qitem is None:
-                # None in the queue indicates the parent want us to exit
-                tasks_queue.task_done()
-                break
-            self.run_one_item(qitem)
-            tasks_queue.task_done()
-
-    def enqueue(self, query_samples):
-        idx = [q.index for q in query_samples]
-        query_id = [q.id for q in query_samples]
-        if len(query_samples) < self.max_batchsize:
-            data, label = self.ds.get_samples(idx)
-            self.tasks.put(Item(query_id, idx, data, label))
-        else:
-            bs = self.max_batchsize
-            for i in range(0, len(idx), bs):
-                ie = i + bs
-                data, label = self.ds.get_samples(idx[i:ie])
-                self.tasks.put(Item(query_id[i:ie], idx[i:ie], data, label))
-
-    def finish(self):
-        # exit all threads
-        for _ in self.workers:
-            self.tasks.put(None)
-        for worker in self.workers:
-            worker.join()
+    return get_backend(backend)
 
 
 def add_results(final_results, name, result_dict, result_list, took, show_accuracy=False):
@@ -384,17 +219,18 @@
     acc_str = ""
     if show_accuracy:
         result["accuracy"] = 100. * result_dict["good"] / result_dict["total"]
-        acc_str = ", acc={:.3f}%".format(result["accuracy"])
+        acc_str = ", acc={:.2f}, good={}, total={}".format(result["accuracy"], result_dict["good"], result_dict["total"])
         if "mAP" in result_dict:
-            result["mAP"] = 100. * result_dict["mAP"]
-            acc_str += ", mAP={:.3f}%".format(result["mAP"])
+            result["mAP"] = result_dict["mAP"]
+            acc_str += ", mAP={:.2f}".format(result_dict["mAP"])
 
     # add the result to the result dict
     final_results[name] = result
 
-    # to stdout
-    print("{} qps={:.2f}, mean={:.4f}, time={:.3f}{}, queries={}, tiles={}".format(
-        name, result["qps"], result["mean"], took, acc_str,
+    # Here we are only showing the accuracy, performance will be calculated as
+    # part of C++ library
+    print("{} {}, tiles={}".format(
+        name, acc_str,
         len(result_list), buckets_str))
 
 
@@ -405,7 +241,7 @@
     log.info(args)
 
     # find backend
-    backend = get_backend(args.backend)
+    backend = get_backend_snpe(args.backend, args)
 
     # override image format if given
     image_format = args.data_format if args.data_format else backend.image_format()
@@ -416,6 +252,9 @@
     count = args.count
     if count:
         count_override = True
+    if not count:
+        if not args.accuracy:
+            count = 200
 
     # dataset to use
     wanted_dataset, pre_proc, post_proc, kwargs = SUPPORTED_DATASETS[args.dataset]
@@ -445,6 +284,21 @@
         os.makedirs(output_dir, exist_ok=True)
         os.chdir(output_dir)
 
+    if args.scenario is "MultiStream" and args.accuracy and backend == "snpe":
+        print("In MultiStream mode accuracy won't work on SNPE")
+        sys.exit(1)
+
+    # Here we actually do the inference in the device to predict results fast.
+    if isinstance(backend, BackendSNPE):
+        backend.execute(ds.image_list)
+
+    # If not accuracy, for any scenario, C++ library will be doing the test
+    if not args.accuracy and isinstance(backend, BackendSNPE):
+        backend.pull_mlperf_logs()
+        print("{}: PerformanceMode test has successfully completed,"
+              " performance logs are generated in mlperf_log_summary.txt"
+              " and mlperf_log_detail.txt".format(args.scenario))
+        return
     #
     # make one pass over the dataset to validate accuracy
     #
@@ -504,7 +358,6 @@
         settings.multi_stream_samples_per_query = args.samples_per_query
     if args.max_latency:
         settings.server_target_latency_ns = int(args.max_latency * NANO_SEC)
-        settings.multi_stream_target_latency_ns = int(args.max_latency * NANO_SEC)
 
     sut = lg.ConstructSUT(issue_queries, flush_queries, process_latencies)
     qsl = lg.ConstructQSL(count, min(count, 500), ds.load_query_samples, ds.unload_query_samples)
