# snpe-mlperf-net-run

## Prerequisites

### Qualcomm Neural Processing SDK
Download the Qualcomm Neural Processing SDK from Qualcomm Developer Network:

https://developer.qualcomm.com/software/qualcomm-neural-processing-sdk

```
mv ~/Downloads/snpe-a.b.c.zip .
unzip snpe-a.b.c.zip
export SNPE_ROOT=`pwd`
```

## Datasets
| dataset | download link |
| ---- | ---- |
| imagenet2012 (validation) | http://image-net.org/challenges/LSVRC/2012/ |
| coco (validation) | http://images.cocodataset.org/zips/val2017.zip |

## Using CK Tool

Steps to install Datasets using CK tool are shared here
https://github.com/mlperf/inference/blob/master/v0.5/classification_and_detection/README.md#using-collective-knowledge-ck

## Model conversion to dlc
To run using snpe all the models has to be converted to dlc, steps to convert models to dlc are shared in the documentation
[README_CONVERTERS.md](README_CONVERTERS.md)

## Build

```
make
```

## Running the benchmark
### Required exports
2 environmental variables needed to export dlc dir and data dir
```
export MODEL_DIR=YourdlcFileLocation
export DATA_DIR=YourImageNetLocation
```
If more than one device connected specify device id on which benchmark to run
```
export DEVICE_ID=YourDeviceIdInformation
```

```
./run_local.sh backend model device

backend is one of [tf|onnxruntime|pytorch|tflite|snpe]
model is one of [resnet50|mobilenet|ssd-mobilenet]
device is one of [cpu|gpu|dsp|aip]

For example:

./run_local.sh snpe mobilenet dsp
```
More options to run benchmark are shared here
https://github.com/mlperf/inference/blob/master/v0.5/classification_and_detection/README.md#examples-for-testing

## Use Jupyter notebook Setup
All the steps mentioned above for running benchmark are captured in the GettingStarted.ipynb
```
cd jni/mlperf_snpe
jupyter notebook
```
