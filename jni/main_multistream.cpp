//==============================================================================
//
// Copyright (c) 2019 Qualcomm Innovation Center, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted (subject to the limitations in the
// disclaimer below) provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution.
//
//    * Neither the name Qualcomm Innovation Center nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
// GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
// HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//==============================================================================
//
// This file contains an application that loads and executes a neural network
// using the SNPE C++ API and saves the MLPerf output to a file.
// Inputs to and outputs from the network are conveyed in binary form as single
// precision floating point values.
//

#include <getopt.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <numeric>
#include <algorithm>
#include <cerrno>
#include <cstdlib>
#include <cstdio>
#include <string>

#include <ctime>
#include <chrono>
#include <unordered_map>
#include <unistd.h>
#include <DlSystem/DlEnums.hpp>
#include <queue>
#include "DlSystem/DlError.hpp"

#include "SNPE/SNPEFactory.hpp"
#include "DlContainer/IDlContainer.hpp"
#include "DlSystem/ITensorFactory.hpp"
#include "DlSystem/StringList.hpp"
#include "DlSystem/TensorMap.hpp"
#include "DlSystem/TensorShape.hpp"
#include "DlSystem/IUserBufferFactory.hpp"
#include "BulkSNPE/BulkSNPE.hpp"
#include "BulkSNPE/UserBufferList.hpp"
#include "BulkSNPE/RuntimeConfigList.hpp"
#include "DlSystem/RuntimeList.hpp"

#include "loadgen.h"
#include "logging.h"
#include "system_under_test.h"
#include "query_sample_library.h"
#include "test_settings.h"

static void ProcessCommandLine(int argc, char **argv);
static void ShowHelp(void);

// Command line settings
std::string ContainerPath;
static std::vector<zdl::DlSystem::Runtime_t> Runtimes;
static std::vector<zdl::DlSystem::RuntimeList> RuntimesListVector;
static std::string OutputDir = "output";
static std::string ModelName;
static std::string MLPerfConfig;
static const std::string MLPerfTestScenario = "SingleStream";
static std::string InputList;
static std::vector<zdl::DlSystem::PerformanceProfile_t> PerfProfile;
static std::vector<bool> cpuFallBacks;
static bool usingTf8UserBuffer = true;
static bool IsFindPeakPerformanceMode = false;
static bool IsAccuracy = false;
static int SamplesPerQuery = -1;
static int TargetQPS = -1;
static zdl::BulkSNPE::UserBufferList inputMaps, outputMaps;
static std::unique_ptr<zdl::BulkSNPE::BulkSNPE> bulksnpe(new zdl::BulkSNPE::BulkSNPE());

// Function forward declaration
void FeedForward(const std::vector<mlperf::QuerySample> &samples);
void LoadSamples(const std::vector<mlperf::QuerySampleIndex> &samples);

void PrintErrorStringAndExit() {
   const char* const errStr = zdl::DlSystem::getLastErrorString();
   std::cerr << errStr << std::endl;
   std::exit(EXIT_FAILURE);
}

class SystemUnderTestTrampoline : public mlperf::SystemUnderTest {
public:
   SystemUnderTestTrampoline() {}

   ~SystemUnderTestTrampoline() override = default;

   const std::string &Name() const override { return name_; }

   void IssueQuery(const std::vector<mlperf::QuerySample> &samples) override {
      FeedForward(samples);
   }

   void ReportLatencyResults(
         const std::vector<mlperf::QuerySampleLatency> &latencies_ns) override {
   }

   void FlushQueries() override {}

private:
   std::string name_;
};

class QuerySampleLibraryTrampoline : public mlperf::QuerySampleLibrary {
public:
   QuerySampleLibraryTrampoline(
         std::string name, size_t total_sample_count,
         size_t performance_sample_count)
         : name_(std::move(name)),
         total_sample_count_(total_sample_count),
         performance_sample_count_(performance_sample_count) {}

   ~QuerySampleLibraryTrampoline() override = default;

   const std::string &Name() const override { return name_; }

   size_t TotalSampleCount() override { return total_sample_count_; }

   size_t PerformanceSampleCount() override { return performance_sample_count_; }

   void LoadSamplesToRam(const std::vector<mlperf::QuerySampleIndex> &samples) override {
   }

   void UnloadSamplesFromRam(
         const std::vector<mlperf::QuerySampleIndex> &samples) override {
   }

private:
   std::string name_;
   size_t total_sample_count_;
   size_t performance_sample_count_;
};

bool FloatToTf8(uint8_t* out,
                unsigned char& stepEquivalentTo0,
                float& quantizedStepSize,
                bool dynamicQuantization,
                float* in,
                size_t numInElement,
                size_t dataBegin)
{
   double encodingMin;
   double encodingMax;
   double encodingRange;
   if(dynamicQuantization)
   {
      float trueMin = std::numeric_limits <float>::max();
      float trueMax = std::numeric_limits <float>::min();

      for (size_t i = 0; i < numInElement; ++i) {
         trueMin = fmin(trueMin, in[i]);
         trueMax = fmax(trueMax, in[i]);
      }

      double stepCloseTo0;

      if (trueMin > 0.0f) {
         stepCloseTo0 = 0.0;
         encodingMin = 0.0;
         encodingMax = trueMax;
      } else if (trueMax < 0.0f) {
         stepCloseTo0 = 255.0;
         encodingMin = trueMin;
         encodingMax = 0.0;
      } else {
         double trueStepSize = static_cast <double>(trueMax - trueMin) / 255.0;
         stepCloseTo0 = -trueMin / trueStepSize;
         if (stepCloseTo0==round(stepCloseTo0)) {
            // 0.0 is exactly representable
            encodingMin = trueMin;
            encodingMax = trueMax;
         } else {
            stepCloseTo0 = round(stepCloseTo0);
            encodingMin = (0.0 - stepCloseTo0) * trueStepSize;
            encodingMax = (255.0 - stepCloseTo0) * trueStepSize;
         }
      }

      const double minEncodingRange = 0.01;
      encodingRange = encodingMax - encodingMin;
      quantizedStepSize = encodingRange / 255.0;
      stepEquivalentTo0 = static_cast <unsigned char> (round(stepCloseTo0));

      if (encodingRange < minEncodingRange) {
         std::cerr << "Expect the encoding range to be larger than " <<  minEncodingRange << "\n"
                   << "Got: " << encodingRange << "\n";
         return false;
      }
   } else {
         // Use the values for quantizedStepSize and stepEquivalentTo0 that were
         // passed into the function, and do not look at "in" to determine these.
         encodingMin = (0 - stepEquivalentTo0) * quantizedStepSize;
         encodingMax = (255 - stepEquivalentTo0) * quantizedStepSize;
         encodingRange = encodingMax - encodingMin;
      }
   for (size_t i = 0; i < numInElement; ++i) {
      int quantizedValue = round(255.0 * (in[i] - encodingMin) / encodingRange);

      if (quantizedValue < 0)
         quantizedValue = 0;
      else if (quantizedValue > 255)
         quantizedValue = 255;

      out[dataBegin++] = static_cast <uint8_t> (quantizedValue);
   }
   return true;
}

void Tf8ToFloat(float *out,
                uint8_t *in,
                const unsigned char stepEquivalentTo0,
                const float quantizedStepSize,
                size_t numElement)
{
   for (size_t i = 0; i < numElement; ++i) {
      double quantizedValue = static_cast <double> (in[i]);
      double stepEqTo0 = static_cast <double> (stepEquivalentTo0);
      out[i] = static_cast <double> ((quantizedValue - stepEqTo0) * quantizedStepSize);
   }
}

size_t calcSizeFromDims(const zdl::DlSystem::Dimension *dims, size_t rank, size_t elementSize )
{
   if (rank == 0) return 0;
   size_t size = elementSize;
   while (rank--) {
      size *= *dims;
      dims++;
   }
   return size;
}

// Helper for splitting tokenized strings
void split(std::vector<std::string>& split_string, const std::string& tokenized_string, const char separator)
{
   split_string.clear();
   std::istringstream tokenized_string_stream(tokenized_string);

   while (!tokenized_string_stream.eof())
   {
      std::string value;
      getline(tokenized_string_stream, value, separator);
      if (!value.empty())
      {
         split_string.push_back(value);
      }
   }
}

bool EnsureDirectory(const std::string& dir)
{
   auto i = dir.find_last_of('/');
   std::string prefix = dir.substr(0, i);

   if (dir.empty() || dir == "." || dir == "..") {
      return true;
   }

   if (i != std::string::npos && !EnsureDirectory(prefix)) {
      return false;
   }

   int rc = mkdir(dir.c_str(),  S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
   if (rc == -1 && errno != EEXIST) {
      return false;
   }
   else {
      struct stat st;
      if (stat(dir.c_str(), &st) == -1) {
         return false;
      }

      return S_ISDIR(st.st_mode);
   }
}

bool loadByteDataFileBatchedTf8(const std::string& inputFile, std::vector<uint8_t>& loadVector, size_t offset, bool dynamicQuantization, unsigned char& stepEquivalentTo0, float& quantizedStepSize)
{
   std::ifstream in(inputFile, std::ifstream::binary);
   std::vector<float> inVector;
   if (!in.is_open() || !in.good())
   {
      std::cerr << "Failed to open input file: " << inputFile << "\n";
   }

   in.seekg(0, in.end);
   size_t length = in.tellg();
   in.seekg(0, in.beg);

   if (loadVector.size() == 0) {
      loadVector.resize(length / sizeof(uint8_t));
   } else if (loadVector.size() < length/sizeof(float)) {
      std::cerr << "Vector is not large enough to hold data of input file: " << inputFile << "\n";
   }

   //inVector.resize((offset+1) * length / sizeof(uint8_t));
   //if (!in.read( reinterpret_cast<char*> (&inVector[offset * length/ sizeof(uint8_t) ]), length) )
   inVector.resize(length/sizeof(float));
   if(!in.read(reinterpret_cast<char*>(&inVector[0]), length))
   {
      std::cerr << "Failed to read the contents of: " << inputFile << "\n";
   }
   size_t databegin = offset*length/(sizeof(float));
   if(!FloatToTf8(loadVector.data(), stepEquivalentTo0, quantizedStepSize, dynamicQuantization, inVector.data(), inVector.size(), databegin))
   {
       return false;
   }
   return true;
}

template<typename T>
bool loadByteDataFileBatched(const std::string& inputFile, std::vector<T>& loadVector, size_t offset)
{
    std::ifstream in(inputFile, std::ifstream::binary);
    if (!in.is_open() || !in.good())
    {
        std::cerr << "Failed to open input file: " << inputFile << "\n";
    }

    in.seekg(0, in.end);
    size_t length = in.tellg();
    in.seekg(0, in.beg);

    if (length % sizeof(T) != 0) {
        std::cerr << "Size of input file should be divisible by sizeof(dtype).\n";
        return false;
    }

    if (loadVector.size() == 0) {
        loadVector.resize(length / sizeof(T));
    } else if (loadVector.size() < length / sizeof(T)) {
        std::cerr << "Vector is not large enough to hold data of input file: " << inputFile << "\n";
    }

    loadVector.resize( (offset+1) * length / sizeof(T) );

    if (!in.read( reinterpret_cast<char*> (&loadVector[offset * length/ sizeof(T) ]), length) )
    {
        std::cerr << "Failed to read the contents of: " << inputFile << "\n";
    }
    return true;
}

bool SaveUserBuffer(const std::string& path, const unsigned char* buffer, size_t size)
{
   // Create the directory path if it does not exist
   auto idx = path.find_last_of('/');
   if (idx != std::string::npos) {
      std::string dir = path.substr(0, idx);
      if (!EnsureDirectory(dir)) {
         std::cerr << "Failed to create output directory: " << dir << ": "
                   << std::strerror(errno) << "\n";
         return false;
      }
   }

   std::ofstream os(path, std::ofstream::binary);
   if (!os) {
      std::cerr << "Failed to open output file for writing: " << path << "\n";
      return false;
   }

   if(!os.write((const char*)buffer, size)) {
      std::cerr << "Failed to write data to: " << path << "\n";
      return false;
   }
   return true;
}
zdl::DlSystem::StringList ResolveOutputLayerNames(std::vector<std::string> &lines) {
   zdl::DlSystem::StringList outputLayers;
   if (!lines.empty() && lines.at(0).compare(0, 1, "#") == 0) {
      std::vector<std::string> names;
      split(names, lines.at(0).substr(1), ' ');
      for (auto& name : names)
         outputLayers.append(name.c_str());
   }
   return outputLayers;
}

std::tuple<std::vector<std::string>, bool> ReadInputList( const std::string filePath ) {
   std::ifstream fileListStream(filePath);
   std::vector<std::string> dummy; // dummy vector for return on failure
   if (!fileListStream)
   {
      std::cerr << "Failed to open input file: " << filePath << "\n";
      return std::make_tuple(dummy, false);
   }

   std::string fileLine;
   std::vector<std::string> lines;
   while (std::getline(fileListStream, fileLine))
   {
      if (fileLine.empty()) continue;
      lines.push_back(fileLine);
   }
   return std::make_tuple(lines ,true);
}

std::vector<std::vector<std::string>> preprocessInput(const std::vector<std::string>& lines, size_t batchSize) {
    // Store batches of inputs into vectors of strings
    std::vector<std::vector<std::string>> result;
    std::vector<std::string> batch;
    for(size_t i=0; i<lines.size(); i++) {
        if(batch.size()==batchSize) {
            result.push_back(batch);
            batch.clear();
        }
        batch.push_back(lines[i]);
    }
    result.push_back(batch);
    return result;
}

void createUserBuffer(zdl::DlSystem::UserBufferMap& userBufferMap,
                      std::unordered_map<std::string, std::vector<uint8_t>>& applicationBuffers,
                      std::vector<std::unique_ptr<zdl::DlSystem::IUserBuffer>>& snpeUserBackedBuffers,
                      std::unique_ptr<zdl::BulkSNPE::BulkSNPE>& bulksnpe,
                      const char * name,
                      const bool isTf8Buffer)
{
   // calculate the size of buffer required by the input tensor
   const zdl::DlSystem::TensorShape bufferShape =  bulksnpe->getBufferAttributesDims(name);

   // Calculate the stride based on buffer strides.
   // Note: Strides = Number of bytes to advance to the next element in each dimension.
   // For example, if a float tensor of dimension 2x4x3 is tightly packed in a buffer of 96 bytes, then the strides would be (48,12,4)
   // Note: Buffer stride is usually known and does not need to be calculated.
   std::vector<size_t> strides(bufferShape.rank());
   strides[strides.size() - 1] = isTf8Buffer ? sizeof(uint8_t) : sizeof(float);
   size_t stride = strides[strides.size() - 1];
   for (size_t i = bufferShape.rank() - 1; i > 0; i--)
   {
      stride *= bufferShape[i];
      strides[i-1] = stride;
   }

   const size_t bufferElementSize = isTf8Buffer ? sizeof(uint8_t) : sizeof(float);
   size_t bufSize = calcSizeFromDims(bufferShape.getDimensions(), bufferShape.rank(), bufferElementSize);
   // set the buffer encoding type
   std::unique_ptr<zdl::DlSystem::UserBufferEncoding> userBufferEncoding;
   if (bufferElementSize == sizeof(uint8_t))
   {
      userBufferEncoding = std::unique_ptr<zdl::DlSystem::UserBufferEncodingTf8>(new zdl::DlSystem::UserBufferEncodingTf8(0,1.0));
   }
   else
   {
      userBufferEncoding = std::unique_ptr<zdl::DlSystem::UserBufferEncodingFloat>(new zdl::DlSystem::UserBufferEncodingFloat());
   }

   // create user-backed storage to load input data onto it
   applicationBuffers.emplace(name, std::vector<uint8_t>(bufSize));

   // create SNPE user buffer from the user-backed buffer
   zdl::DlSystem::IUserBufferFactory& ubFactory = zdl::SNPE::SNPEFactory::getUserBufferFactory();
   snpeUserBackedBuffers.push_back(ubFactory.createUserBuffer(applicationBuffers.at(name).data(),
                                                              bufSize,
                                                              strides,
                                                              userBufferEncoding.get()));
   if (snpeUserBackedBuffers.back() == nullptr)
   {
      std::cerr << "Error while creating user buffer." << std::endl;
   }
   // add the user-backed buffer to the inputMap, which is later on fed to the network for execution
   userBufferMap.add(name, snpeUserBackedBuffers.back().get());
}

bool loadInputUserBuffer(std::unordered_map<std::string, std::vector<uint8_t>>& applicationBuffers,
                         std::unique_ptr<zdl::BulkSNPE::BulkSNPE>& bulksnpe,
                         std::vector<std::string>& fileLines,
                         zdl::DlSystem::UserBufferMap& inputMap,
                         const bool isTf8Buffer)
{
    // get input tensor names of the network that need to be populated
    const zdl::DlSystem::StringList inputNames= bulksnpe->getInputTensorNames();
    //if (inputNames.size()) std::cout << "Processing begin: " << std::endl;

    for(size_t i=0; i<fileLines.size(); i++) {
        std::string fileLine(fileLines[i]);
        // treat each line as a space-separated list of input files
        std::vector<std::string> filePaths;
        split(filePaths, fileLine, ' ');

        for (size_t j = 0; j < inputNames.size(); j++) {
            const char *name = inputNames.at(j);
            std::string filePath(filePaths[j]);
            // load file content onto application storage buffer,
            // on top of which, SNPE has created a user buffer
            if(isTf8Buffer)
            {
               unsigned char stepEquivalentTo0;
               float quantizedStepSize;
               bool dynamicQuantization = true;
               if(!loadByteDataFileBatchedTf8(filePath, applicationBuffers.at(name), i, dynamicQuantization, stepEquivalentTo0, quantizedStepSize))
               {
                   return false;
               }
               auto userBufferEncoding = dynamic_cast<zdl::DlSystem::UserBufferEncodingTf8 *>(&inputMap.getUserBuffer(name)->getEncoding());
               userBufferEncoding->setStepExactly0(stepEquivalentTo0);
               userBufferEncoding->setQuantizedStepSize(quantizedStepSize);
            } else {
               if(!loadByteDataFileBatched(filePath, applicationBuffers.at(name), i))
               {
                   return false;
               }
            }
        }
    }
    return true;
}

// Execute the network on an input user buffer map and print results to raw files
bool saveOutput (zdl::DlSystem::UserBufferMap& outputMap,
                 std::unordered_map<std::string,std::vector<uint8_t>>& applicationOutputBuffers,
                 const std::string& outputDir,
                 int num,
                 size_t batchSize,
                 bool /* isTf8Buffer */)
{
   // Get all output buffer names from the network
   const zdl::DlSystem::StringList& outputBufferNames = outputMap.getUserBufferNames();

   // Iterate through output buffers and print each output to a raw file
   for(auto & name : outputBufferNames )
   {
      for(size_t j=0; j<batchSize; j++) {
         std::ostringstream path;
         path << outputDir << "/"
              << "Result_" << num + j << "/"
              << name << ".raw";
         std::string outputPath = path.str();
         std::string::size_type pos = outputPath.find(":");
         if(pos != std::string::npos) outputPath = outputPath.replace(pos, 1, "_");
         auto bufSize = outputMap.getUserBuffer(name)->getSize();
         auto dataSize = outputMap.getUserBuffer(name)->getOutputSize();
         const zdl::DlSystem::UserBufferEncoding& ubEncoding = outputMap.getUserBuffer(name)->getEncoding();
         const zdl::DlSystem::UserBufferEncodingFloat* ubEncodingFloat
            = dynamic_cast <const zdl::DlSystem::UserBufferEncodingFloat*> (&ubEncoding);
         size_t tensorStart = j * bufSize / batchSize;
         size_t tensorSize =  bufSize / batchSize;
         if (ubEncodingFloat!=nullptr) {
            if(bufSize != dataSize) {
               std::cout << "UserBuffer size is " << bufSize << " bytes, but "
                         << dataSize << " bytes of data was found." << std::endl;
               if( dataSize > bufSize )
                  std::cout << "Assign a larger buffer using a bigger --resizable_dim argument" << std::endl;
               tensorStart = j * std::min(bufSize,dataSize) / batchSize;
               tensorSize = std::min(bufSize,dataSize) / batchSize;
            }
            if(!SaveUserBuffer(outputPath, applicationOutputBuffers.at(name).data() + tensorStart, tensorSize))
            {
                return false;
            }
         }
         else {
            const zdl::DlSystem::UserBufferEncodingTf8* ubEncodingTf8
               = dynamic_cast <const zdl::DlSystem::UserBufferEncodingTf8*> (&ubEncoding);
            if (ubEncodingTf8!=nullptr) {
               size_t numElement = bufSize/sizeof(uint8_t);
               if(bufSize != dataSize) {
                  std::cout << "UserBuffer size is " << bufSize << " bytes, but "
                            << dataSize << " bytes of data was found." << std::endl;
                  if( dataSize > bufSize )
                     std::cout << "Assign a larger buffer using a bigger --resizable_dim argument" << std::endl;
                  tensorStart = j * std::min(bufSize,dataSize) / batchSize;
                  numElement = std::min(bufSize,dataSize) / sizeof(uint8_t);
               }
               std::vector <float> outputVector (numElement);
               // external dequantizer
               const unsigned char stepEquivalentTo0 = ubEncodingTf8->getStepExactly0();
               const float quantizedStepSize = ubEncodingTf8->getQuantizedStepSize();
               Tf8ToFloat(outputVector.data(), applicationOutputBuffers.at(name).data(), stepEquivalentTo0,
                           quantizedStepSize, numElement);
               const unsigned char* dataPtr = (const unsigned char*) (outputVector.data());
               if(!SaveUserBuffer(outputPath, dataPtr + tensorStart * sizeof(float), numElement * sizeof(float) / batchSize))
               {
                   return false;
               }
            }
            else {
               std::cerr << "Invalid output tensor type." << "\n";
               return false;
            }
         }
      }
   }
   return true;
}

int main(int argc, char **argv) {
    ProcessCommandLine(argc, argv);
    // Read the container
    std::unique_ptr<zdl::DlContainer::IDlContainer> container;
    container = zdl::DlContainer::IDlContainer::open(ContainerPath);
    if (!container) {
       std::cerr << "Error: could not open your model. "
                 << "Please check your DLContainer." << std::endl;
       return EXIT_FAILURE;
    }

    size_t numRequestedInstances = 0;
    bool isRuntimeListValid = false;

    if(RuntimesListVector.empty())
    {
        numRequestedInstances = Runtimes.size();
    }
    else
    {
        numRequestedInstances = RuntimesListVector.size();
        isRuntimeListValid = true;
    }

    // First attempt to create as many SNPE instances as requested. If we cannot create
    // as many requested instances, create as many as possible and continue.
    size_t numCreatedInstances = 0;
    zdl::BulkSNPE::RuntimeConfigList runtimeconfigs;
    for (size_t j = 0; j < numRequestedInstances; j++) {
        zdl::BulkSNPE::RuntimeConfig runtimeConfig;
        if(isRuntimeListValid == false)
        {
            // Check the runtime
            if (!zdl::SNPE::SNPEFactory::isRuntimeAvailable(Runtimes[j])) {
                std::cout << "The selected runtime is not available on this platform. Continue anyway "
                          << "to observe the failure at network creation time." << std::endl;
            }
            runtimeConfig.runtime = Runtimes[j];
            runtimeConfig.enableCPUFallback = cpuFallBacks[j];
        }
        else
        {
            for(size_t idx = 0; idx < RuntimesListVector[j].size(); idx++)
            {
               // Check the runtime
               if (!zdl::SNPE::SNPEFactory::isRuntimeAvailable(RuntimesListVector[j][idx])) {
                   std::cout << "The selected runtime is not available on this platform. Continue anyway "
                             << "to observe the failure at network creation time." << std::endl;
               }
            }
            runtimeConfig.runtimeList = RuntimesListVector[j];
        }

        runtimeConfig.perfProfile = PerfProfile[j];
        runtimeconfigs.push_back(runtimeConfig);
        numCreatedInstances++;
    }

    std::vector<std::string> lines;
    bool readSuccess;
    std::tie(lines, readSuccess) = ReadInputList(InputList);
    if(!readSuccess)
    {
        return EXIT_FAILURE;
    }

    zdl::DlSystem::StringList outputLayers;
    outputLayers = ResolveOutputLayerNames(lines);
    if (!lines.empty() && lines.at(0).compare(0, 1, "#") == 0) {
        lines.erase(lines.begin());
    }

    zdl::BulkSNPE::BuildConfig buildConfig;
    buildConfig.container = container.get();
    buildConfig.runtimeConfigList = runtimeconfigs;
    buildConfig.outputBufferNames = outputLayers;
    std::chrono::time_point<std::chrono::high_resolution_clock> chronoStart, chronoStop;
    chronoStart = std::chrono::high_resolution_clock::now();
    std::cout << "BulkSNPE build started" << std::endl;
    bool buildStatus = bulksnpe->build(buildConfig);
    std::cout << "BulkSNPE build finished" << std::endl;

    chronoStop = std::chrono::high_resolution_clock::now();
    double buildTime = std::chrono::duration_cast<std::chrono::microseconds>(chronoStop-chronoStart).count() / 1000000.0; //ms
    if(!buildStatus)
    {
        std::cerr << "Error: bulk snpe build failed. " << std::endl;
        return EXIT_FAILURE;
    }
    if (numRequestedInstances != numCreatedInstances) {
        std::cout << "Warning: Requested " << numRequestedInstances << " instances of SNPE, "
                  << "only able to create " << numCreatedInstances << " instances. "
                  << "Continuing to execution with created instances." << std::endl;
    }

    // Open the input file listing and group input files into batches
    zdl::DlSystem::TensorShape tensorshape;
    tensorshape = bulksnpe->getInputDimensions();
    size_t batchSize = tensorshape.getDimensions()[0];
    std::cout << "Batch size for the container is " << batchSize << std::endl;
    std::vector<std::vector<std::string>> inputs = preprocessInput(lines, batchSize);
    size_t nums = inputs.size();
    std::cout << "the number of input data is: " << nums << std::endl;
    std::vector<std::unordered_map <std::string, std::vector<uint8_t>>> outputBuffersVec(nums);
    std::vector<std::unordered_map <std::string, std::vector<uint8_t>>> inputBuffersVec(nums);
    std::vector <std::unique_ptr<zdl::DlSystem::IUserBuffer>> snpeUserBackedInputBuffers, snpeUserBackedOutputBuffers;
    zdl::BulkSNPE::UserBufferList inputMapList(nums), outputMapList(nums);

    const zdl::DlSystem::StringList innames = bulksnpe->getInputTensorNames();
    const zdl::DlSystem::StringList outnames = bulksnpe->getOutputTensorNames();
    for(size_t i = 0; i < inputs.size(); ++i)
    {
        for (const char *name : innames) {
            createUserBuffer(inputMapList[i], inputBuffersVec[i], snpeUserBackedInputBuffers, bulksnpe, name, usingTf8UserBuffer);
        }
        for (const char *name : outnames) {
            createUserBuffer(outputMapList[i], outputBuffersVec[i], snpeUserBackedOutputBuffers, bulksnpe, name, usingTf8UserBuffer);
        }
        if(!loadInputUserBuffer(inputBuffersVec[i], bulksnpe, inputs[i], inputMapList[i], usingTf8UserBuffer))
        {
            return EXIT_FAILURE;
        }
    }

    // Do a warm up calling once the execute
    bool exeStatus = bulksnpe->execute(inputMapList, outputMapList);
    if (!exeStatus)
    {
      std::cout << "failed to execute bulksnpe." << std::endl;
      PrintErrorStringAndExit();
    }

    if (IsAccuracy) {
       std::cout << "Generating outputs in bulksnpe mode " << std::endl;
       for(size_t i = 0; i < nums; i++) {
          if(!saveOutput(outputMapList[i], outputBuffersVec[i], OutputDir, i * batchSize, batchSize, true)) {
             PrintErrorStringAndExit();
          }
       }
       return 0;
    }
    inputMaps = inputMapList;
    outputMaps = outputMapList;

    std::cout << "Starting mlperf testing" << std::endl;
    int imagesCount = inputs.size();
    // ML Perf test settings
    SystemUnderTestTrampoline* sut = new SystemUnderTestTrampoline();
    QuerySampleLibraryTrampoline* qsl = new QuerySampleLibraryTrampoline("Query Sample library test",
       imagesCount, std::min(imagesCount, 1024));
    mlperf::LogSettings default_log_settings;
    default_log_settings.enable_trace = false;
    mlperf::TestSettings testSettings;
    testSettings.FromConfig(MLPerfConfig, ModelName, MLPerfTestScenario);
    testSettings.scenario =  mlperf::TestScenario::MultiStream;
    if (IsFindPeakPerformanceMode) {
       testSettings.mode =  mlperf::TestMode::FindPeakPerformance;
    }
    if (SamplesPerQuery != -1) {
       testSettings.multi_stream_samples_per_query = SamplesPerQuery;
    }
    if (TargetQPS != -1) {
       testSettings.multi_stream_target_qps = TargetQPS;
    }
    mlperf::StartTest(sut, qsl, testSettings, default_log_settings);
    delete sut;
    delete qsl;
    return 0;
}

void FeedForward(const std::vector<mlperf::QuerySample> &samples) {
   std::vector<mlperf::QuerySampleResponse> responses;
   int nums = samples.size();
   zdl::BulkSNPE::UserBufferList inputMapList(nums), outputMapList(nums);
   for (int i = 0; i < nums; i++) {
      mlperf::QuerySampleResponse response;
      response.id = samples[i].id;
      responses.push_back(response);
      inputMapList[i] = inputMaps[samples[i].index];
      outputMapList[i] = outputMaps[samples[i].index];
   }

   //bulk snpe execute.
   bool exeStatus = bulksnpe->execute(inputMapList, outputMapList);
   if (!exeStatus)
   {
      std::cout << "failed to execute bulksnpe." << std::endl;
      PrintErrorStringAndExit();
   }
   mlperf::QuerySamplesComplete(responses.data(), responses.size());
}

void ProcessCommandLine(int argc, char **argv) {
    enum OPTIONS {
        OPT_HELP = 0,
        OPT_CONTAINER = 1,
        OPT_IUTPUT_LIST = 2,
        OPT_OUTPUT_DIR = 4,
        OPT_USE_GPU = 6,
        OPT_USE_DSP = 8,
        OPT_USE_AIP = 101,
        OPT_PERF_PROFILE = 9,
        OPT_VERSION = 10,
        OPT_GPU_MODE = 15,
        OPT_USE_CPU = 16,
        OPT_USE_GPU_FP16 = 17,
        OPT_CPU_FALLBACK = 19,
        OPT_USERBUFFER_FLOAT = 12,
        OPT_USERBUFFER_TF8 = 13,
        OPT_RUNTIME_ORDER = 14,
        OPT_MODEL_NAME = 20,
        OPT_MLPERF_CONFIG = 21,
        OPT_FIND_PEAK_PERFORMANCE_MODE = 22,
        OPT_SAMPLES_PER_QUERY = 23,
        OPT_TARGET_QPS = 24,
        OPT_ACCURACY = 25
    };


    // Create the command line options
    static struct option long_options[] = {
            { "help", no_argument, NULL, OPT_HELP},
            { "container", required_argument, NULL, OPT_CONTAINER},
            { "input_list", required_argument, NULL,OPT_IUTPUT_LIST},
            { "output_dir", required_argument, NULL, OPT_OUTPUT_DIR},
            { "use_gpu", no_argument, NULL, OPT_USE_GPU},
            { "use_dsp", no_argument, NULL, OPT_USE_DSP },
            { "use_aip", no_argument, NULL,  OPT_USE_AIP },
            { "perf_profile", required_argument, NULL, OPT_PERF_PROFILE },
            { "version", no_argument, NULL, OPT_VERSION },
            { "gpu_mode", required_argument, NULL, OPT_GPU_MODE },
            { "use_cpu", no_argument, NULL, OPT_USE_CPU },
            { "use_gpu_fp16", no_argument, NULL, OPT_USE_GPU_FP16 },
            { "cpu_fallback", required_argument, NULL, OPT_CPU_FALLBACK },
            { "userbuffer_float", no_argument, NULL, OPT_USERBUFFER_FLOAT },
            { "userbuffer_tf8", no_argument,  NULL,  OPT_USERBUFFER_TF8 },
            { "runtime_order", required_argument, NULL, OPT_RUNTIME_ORDER},
            { "model_name", required_argument, NULL, OPT_MODEL_NAME},
            { "mlperf_config", required_argument, NULL, OPT_MLPERF_CONFIG},
            { "find_peak_performance", no_argument, NULL, OPT_FIND_PEAK_PERFORMANCE_MODE},
            { "samples_per_query", required_argument, NULL, OPT_SAMPLES_PER_QUERY},
            { "target_qps", required_argument, NULL, OPT_TARGET_QPS},
            {"accuracy", no_argument, NULL, OPT_ACCURACY },
            { NULL, 0, NULL, 0 },
    };

    // Command line parsing loop
    int long_index = 0;
    int opt = 0;
    while ((opt = getopt_long_only(argc, argv, "", long_options, &long_index)) != -1) {
        switch (opt) {
            case OPT_HELP:
                ShowHelp();
                std::exit(0);
                break;

            case OPT_ACCURACY:
                IsAccuracy = true;
                break;

            case OPT_CONTAINER:
                ContainerPath = optarg;
                break;

            case OPT_MODEL_NAME:
                ModelName = optarg;
                break;

            case OPT_MLPERF_CONFIG:
                MLPerfConfig = optarg;
                break;

            case OPT_FIND_PEAK_PERFORMANCE_MODE:
                IsFindPeakPerformanceMode = true;
                break;

            case OPT_SAMPLES_PER_QUERY:
                SamplesPerQuery = stoi(std::string(optarg));
                break;

            case OPT_TARGET_QPS:
                TargetQPS = stoi(std::string(optarg));
                break;

            case OPT_IUTPUT_LIST:
                InputList = optarg;
                break;

            case OPT_OUTPUT_DIR:
                OutputDir = optarg;
                break;

            case OPT_USE_CPU:
                Runtimes.push_back(zdl::DlSystem::Runtime_t::CPU);
                break;

            case OPT_USE_GPU:
                Runtimes.push_back(zdl::DlSystem::Runtime_t::GPU);
                break;

            case OPT_USE_GPU_FP16:
                Runtimes.push_back(zdl::DlSystem::Runtime_t::GPU_FLOAT16);
                break;

            case OPT_USE_DSP:
                Runtimes.push_back(zdl::DlSystem::Runtime_t::DSP);
                break;

            case OPT_USE_AIP:
                Runtimes.push_back(zdl::DlSystem::Runtime_t::AIP_FIXED8_TF);
                break;

            case OPT_PERF_PROFILE:
                zdl::DlSystem::PerformanceProfile_t profile;
                if ((std::strcmp("default", optarg) == 0) ||
                    (std::strcmp("balanced", optarg) == 0)) {
                    profile = zdl::DlSystem::PerformanceProfile_t::BALANCED;
                } else if (std::strcmp("high_performance", optarg) == 0) {
                    profile = zdl::DlSystem::PerformanceProfile_t::HIGH_PERFORMANCE;
                } else if (std::strcmp("power_saver", optarg) == 0) {
                    profile = zdl::DlSystem::PerformanceProfile_t::POWER_SAVER;
                } else if (std::strcmp("system_settings", optarg) == 0) {
                    profile = zdl::DlSystem::PerformanceProfile_t::SYSTEM_SETTINGS;
                } else if (std::strcmp("sustained_high_performance", optarg) == 0) {
                    profile = zdl::DlSystem::PerformanceProfile_t::SUSTAINED_HIGH_PERFORMANCE;
                } else if (std::strcmp("burst", optarg) == 0) {
                    profile = zdl::DlSystem::PerformanceProfile_t::BURST;
                }
                else {
                    std::cerr << "Unknown perf profile " << optarg << std::endl;
                    std::exit(EXIT_FAILURE);
                }
                PerfProfile.push_back(profile);
                break;

            case OPT_USERBUFFER_FLOAT:
               usingTf8UserBuffer = false;
               break;

            case OPT_USERBUFFER_TF8:
               usingTf8UserBuffer = true;
               break;

            case OPT_VERSION:
                std::cout << "SNPE v" << zdl::SNPE::SNPEFactory::getLibraryVersion().toString()
                          << std::endl;
                std::exit(0);

            case OPT_CPU_FALLBACK:
                if (std::strcmp("true", optarg) == 0) {
                    cpuFallBacks.push_back(true);
                } else if (std::strcmp("false", optarg) == 0) {
                    cpuFallBacks.push_back(false);
                } else {
                    std::cerr << "Unknown cpu fall back " << optarg << std::endl;
                    std::exit(EXIT_FAILURE);
                }
                break;

            case OPT_RUNTIME_ORDER:
               {
                   std::string inputString = optarg;
                   //std::cout<<"Input String: "<<inputString<<std::endl;
                   std::vector<std::string> runtimeStrVector;
                   split(runtimeStrVector, inputString, ',');
                   zdl::DlSystem::RuntimeList runtimesList;

                   //Check for dups
                   for(auto it = runtimeStrVector.begin(); it != runtimeStrVector.end()-1; it++)
                   {
                      auto found = std::find(it+1, runtimeStrVector.end(), *it);
                      if(found != runtimeStrVector.end())
                      {
                         RuntimesListVector.clear();
                         std::cerr << "Error: Invalid values passed to the argument "<< argv[optind-2] << ". Duplicate entries in runtime order" << std::endl;
                         ShowHelp();
                         std::exit(EXIT_FAILURE);
                      }
                   }

                   for(auto& runtimeStr : runtimeStrVector)
                   {
                      //std::cout<<runtimeStr<<std::endl;
                      zdl::DlSystem::Runtime_t runtime = zdl::DlSystem::RuntimeList::stringToRuntime(runtimeStr.c_str());
                      if(runtime != zdl::DlSystem::Runtime_t::UNSET)
                      {
                         bool ret = runtimesList.add(runtime);
                         if(ret == false)
                         {
                            std::cerr <<zdl::DlSystem::getLastErrorString()<<std::endl;
                            RuntimesListVector.clear();
                            std::cerr << "Error: Invalid values passed to the argument "<< argv[optind-2] << ". Please provide comma seperated runtime order of precedence" << std::endl;
                            ShowHelp();
                            std::exit(EXIT_FAILURE);
                         }
                      }
                      else
                      {
                         RuntimesListVector.clear();
                         std::cerr << "Error: Invalid values passed to the argument "<< argv[optind-2] << ". Please provide comma seperated runtime order of precedence" << std::endl;
                         ShowHelp();
                         std::exit(EXIT_FAILURE);
                      }
                   }
                   RuntimesListVector.push_back(runtimesList);
               }
               break;

            default:
                std::cerr << "Missing option: --container\n" << optarg;
                ShowHelp();
                std::exit(EXIT_FAILURE);
        }
    }

    if (ContainerPath.empty()) {
        std::cerr << "Missing option: --container\n";
        ShowHelp();
        std::exit(EXIT_FAILURE);
    }

    if (InputList.empty()) {
        std::cerr << "Missing option: --input_list\n";
        ShowHelp();
        std::exit(EXIT_FAILURE);
    }

    if ((Runtimes.empty() == false) && (RuntimesListVector.empty() == false)) {
        std::cerr << "Invalid option cannot mix --runtime_order with --use_cpu or --use_dsp or --use_dsp or --use_aip or --use_gpu_fp16 or --use_fxp_cpu\n";
        ShowHelp();
        std::exit(EXIT_FAILURE);
    }

    if (Runtimes.empty() && RuntimesListVector.empty()) {
        std::cerr << "Missing runtime options. Need to specify which runtime to run on.\n";
        ShowHelp();
        std::exit(EXIT_FAILURE);
    }

    if ((Runtimes.empty() && PerfProfile.size() != RuntimesListVector.size()) || //runtimelist-perfProfile mismatch
        (RuntimesListVector.empty() && PerfProfile.size() != Runtimes.size()))   //runtime-perfProfile mismatch
    {
        std::cerr << "The number of performance profiles does not match the number of runtimes.\n";
        ShowHelp();
        std::exit(EXIT_FAILURE);
    }

    if (RuntimesListVector.empty() && cpuFallBacks.size() != Runtimes.size()) {
        std::cerr << "The number of cpuFallBacks does not match the number of runtimes.\n";
        ShowHelp();
        std::exit(EXIT_FAILURE);
    }

    if (Runtimes.empty() && cpuFallBacks.size() != 0) {
        std::cerr << "Argument --cpu_fallback does not does not apply with --runtime_order\n";
        ShowHelp();
        std::exit(EXIT_FAILURE);
    }

}

void ShowHelp(void) {
    std::cout
            << "\nDESCRIPTION:\n"
            << "------------\n"
            << "Example application demonstrating how to use BulkSNPE\n"
            << "using the BulkSNPE and SNPE C++ API.\n"
            << "\n\n"
            << "REQUIRED ARGUMENTS:\n"
            << "-------------------\n"
            << "  --container  <FILE>   Path to the DL container containing the network.\n"
            << "  --input_list <FILE>   Path to the flile containing the input images path.\n"
            << "  --use_cpu             Use the CPU runtime for SNPE.\n"
            << "  --use_gpu             Use the GPU float32 runtime for SNPE.\n"
            << "  --use_gpu_fp16        Use the GPU float16 runtime for SNPE.\n"
            << "  --use_dsp             Use the DSP fixed point runtime for SNPE.\n"
            << "  --use_aip             Use the AIP fixed point runtime for SNPE.\n"
            << "  --runtime_order <VAL,VAL,VAL,..> Specifies the order of precedence for runtime e.g cpu,gpu etc. Valid values are:- \n"
            << "                                   cpu_float32 (Snapdragon CPU)       = Data & Math: float 32bit \n"
            << "                                   gpu_float32_16_hybrid (Adreno GPU) = Data: float 16bit Math: float 32bit \n"
            << "                                   dsp_fixed8_tf (Hexagon DSP)        = Data & Math: 8bit fixed point Tensorflow style format \n"
            << "                                   gpu_float16 (Adreno GPU)           = Data: float 16bit Math: float 16bit \n"
            << "                                   aip_fixed8_tf (Snapdragon HTA+HVX) = Data & Math: 8bit fixed point Tensorflow style format \n"
            << "                                   gpu (Adreno GPU)                   = Same as gpu_float32_16_hybrid \n"
            << "                                   dsp (Hexagon DSP)                  = Same as dsp_fixed8_tf \n"
            << "                                   aip (Snapdragon HTA+HVX)           = Same as aip_fixed8_tf \n"
            << "  --perf_profile <VAL>  Specifies perf profile to set. Valid settings are \"balanced\" , \"default\" , \"high_performance\" , \"sustained_high_performance\" , \"burst\" , \"power_saver\" and \"system_settings\". \n"
            << "                        NOTE: \"balanced\" and \"default\" are the same.  \"default\" is being deprecated in the future.\n"
            << "  --cpu_fallback        Enables cpu fallback functionality. Valid settings are \"false\", \"true\".\n"
            << "  --userbuffer_float    Specifies to use userbuffer for inference, and the input type is float.\n"
            << "                        Cannot be combined with --encoding_type.\n"
            << "  --userbuffer_tf8      Specifies to use userbuffer for inference, and the input type is tf8exact0.\n"
            << "                        Cannot be combined with --encoding_type.\n"
            << "\n\n"
            << "  --model_name          Model name on which TestSettings has to be applied.\n"
            << "  --mlperf_config       mlperf_conf file to set tune the TestSettings.\n"
            << "OPTIONAL ARGUMENTS:\n"
            << "-------------------\n"
            << "                        will be saved.\n"
            << "  --output_dir <DIR>    The directory to save result files \n"
            << "  --version             Show SNPE Version Number. \n"
            << "  --find_peak_performance mlperf setting find peak performance mode with this option. \n"
            << "  --samples_per_query    mlperf test setting to set number of samples per query. \n"
            << "  --target_qps          mlperf test setting to set the target qps using this option. \n"
            << "  --help                Show this help message.\n"
            << "  --accuracy            Selects accuracy over performance.\n"
            << std::endl;
}
