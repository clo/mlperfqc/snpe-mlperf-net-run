# Copyright (c) 2019 Qualcomm Innovation Center, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name Qualcomm Innovation Center nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


LOCAL_PATH := $(call my-dir)

SNPE_INCLUDE_DIR := $(SNPE_ROOT)/include/zdl
SNPE_LIB_DIR := $(SNPE_ROOT)/lib/arm-android-clang6.0
SNPE_DSP_DIR := $(SNPE_ROOT)/lib/dsp

include $(CLEAR_VARS)
LOCAL_MODULE := snpe-mlperf-net-run
LOCAL_SRC_FILES := main.cpp
LOCAL_SHARED_LIBRARIES := libSNPE \
                          libBulkSNPE \
                          libSYMPHONYCPU \
                          libloadgen
LOCAL_LDLIBS     := -lGLESv2 -lEGL
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := snpe-mlperf-multistream-net-run
LOCAL_SRC_FILES := main_multistream.cpp
LOCAL_SHARED_LIBRARIES := libSNPE \
                          libBulkSNPE \
                          libSYMPHONYCPU \
                          libloadgen
LOCAL_LDLIBS     := -lGLESv2 -lEGL
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := libloadgen
LOCAL_SRC_FILES:= ../loadgen_build/libmlperf_loadgen.a
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/../external/mlperf_inference/loadgen
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libSNPE
LOCAL_SRC_FILES := $(SNPE_LIB_DIR)/libSNPE.so
LOCAL_EXPORT_C_INCLUDES += $(SNPE_INCLUDE_DIR)
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libBulkSNPE
LOCAL_SRC_FILES := $(SNPE_LIB_DIR)/libBulkSNPE.so
LOCAL_EXPORT_C_INCLUDES += $(SNPE_INCLUDE_DIR)
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libSYMPHONYCPU
LOCAL_SRC_FILES := $(SNPE_LIB_DIR)/libsymphony-cpu.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libSNPE_ADSP
LOCAL_SRC_FILES := $(SNPE_LIB_DIR)/libsnpe_adsp.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libSNPE_DSP_DOMAINS
LOCAL_SRC_FILES := $(SNPE_LIB_DIR)/libsnpe_dsp_domains.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libSNPE_DSP_DOMAINS_SYSTEM
LOCAL_SRC_FILES := $(SNPE_LIB_DIR)/libsnpe_dsp_domains_system.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libSNPE_DSP_DOMAINS_V2
LOCAL_SRC_FILES := $(SNPE_LIB_DIR)/libsnpe_dsp_domains_v2.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libSNPE_DSP_DOMAINS_V2_SYSTEM
LOCAL_SRC_FILES := $(SNPE_LIB_DIR)/libsnpe_dsp_domains_v2_system.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libHTA
LOCAL_SRC_FILES := $(SNPE_LIB_DIR)/libhta.so
include $(PREBUILT_SHARED_LIBRARY)
