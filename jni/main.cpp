//==============================================================================
//
// Copyright (c) 2019 Qualcomm Innovation Center, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted (subject to the limitations in the
// disclaimer below) provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution.
//
//    * Neither the name Qualcomm Innovation Center nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
// GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
// HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//==============================================================================
//
// This file contains an application that loads and executes a neural network
// using the SNPE C++ API and saves the MLPerf output to a file.
// Inputs to and outputs from the network are conveyed in binary form as single
// precision floating point values.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <cstring>
#include <numeric>
#include <limits>
#include <cerrno>
#include <cstdlib>
#include <queue>
#include <thread>

#include "SNPE/SNPE.hpp"
#include <unordered_map>
#include <DlSystem/IUserBuffer.hpp>
#include <DlSystem/DlEnums.hpp>

#include "SNPE/SNPEFactory.hpp"
#include "SNPE/SNPEBuilder.hpp"
#include "DlContainer/IDlContainer.hpp"
#include "DiagLog/IDiagLog.hpp"
#include "DlSystem/DlError.hpp"
#include "DlSystem/StringList.hpp"
#include "DlSystem/TensorMap.hpp"
#include "DlSystem/TensorShape.hpp"
#include "DlSystem/IUserBuffer.hpp"
#include "DlSystem/IUserBufferFactory.hpp"
#include "DlSystem/UserBufferMap.hpp"
#include "DlSystem/IBufferAttributes.hpp"
#include "DlSystem/TensorShapeMap.hpp"
#include "DlSystem/DlEnums.hpp"
#include "DlSystem/PlatformConfig.hpp"

#include <GLES2/gl2.h>
#include <EGL/egl.h>
#define GL_SHADER_STORAGE_BUFFER 0x90D2

void CreateGLContext();

#include "loadgen.h"
#include "logging.h"
#include "system_under_test.h"
#include "query_sample_library.h"
#include "test_settings.h"

using namespace std;

template <typename Vector>
static std::vector<Vector> LoadInputFile(const std::string& inputFile);

template <typename Vector>
static bool LoadBatchedInput(std::string& path, const size_t& inputSize, std::vector<Vector>& inputVec,
      size_t& numFilesCopied, size_t& batchSize);
static bool SaveUserBuffer(const std::string& path, const unsigned char* buffer, size_t size);
static bool EnsureDirectoryExists(const std::string& dir);
static void ProcessCommandLine(int argc, char** argv);
static void ShowHelp(void);
void FeedForward(const std::vector<mlperf::QuerySample> &samples);
void FeedForwardOneItem(size_t index);
void FeedForwardUserBuffer(size_t index);
void SaveOutputBuffer(int index);
void LoadSamplesToRam();
void LoadSample(int index);
static std::vector<zdl::DlSystem::UserBufferMap> outputMaps, inputMaps;
static std::vector<std::vector<std::unique_ptr<zdl::DlSystem::IUserBuffer>> > ubs_list;
static std::vector<std::unordered_map<std::string, std::vector<uint8_t>> > bufs_list;
static size_t numFilesCopied = 0;
static size_t batchSize = 0;
static std::string uInputName;

zdl::DlSystem::StringList ResolveOutputLayerNames(std::vector<std::string> &lines);

std::pair<std::vector<std::string>,std::vector<std::string>> ParseInputLine(std::string &line);

// Command line settings
static std::string ContainerPath;
static std::string InputFileListPath;
static std::string OutputDir = "./output/";
static std::string StorageDir;
static std::string ModelName;
static std::string MLPerfConfig;
static const std::string MLPerfTestScenario = "SingleStream";
static bool DebugNetworkOutputs;
static zdl::DlSystem::Runtime_t Runtime = zdl::DlSystem::Runtime_t::CPU;
static zdl::DlSystem::ProfilingLevel_t ProfilingLevel = zdl::DlSystem::ProfilingLevel_t::OFF;
static zdl::DlSystem::PerformanceProfile_t PerfProfile = zdl::DlSystem::PerformanceProfile_t::HIGH_PERFORMANCE;
static zdl::DlSystem::TensorShapeMap InputDimensionsMap;
static bool isInputNv21EncodingType = false;
static bool usingFloatUserBuffer = true;
static bool usingTf8UserBuffer = false;
static bool usingDynamicQuantization = true;
static bool usingInitCache = true;
static bool IsCpuFallbackEnabled = false;
static bool IsAccuracyMode = false;
static bool IsMlPerfCompleted = false;
static long SingleStreamMaxLatency = 1000000;
static std::string PlatformOptions = "";
static std::vector<std::vector<std::string>> filePathsList;
static std::vector<zdl::DlSystem::StringList> inputTensorNamesList;
static std::unique_ptr<zdl::SNPE::SNPE> snpe;

// ML Perf test settings
static size_t resizable_dim = 10;


class SystemUnderTestTrampoline : public mlperf::SystemUnderTest {
public:
   SystemUnderTestTrampoline() {}

   ~SystemUnderTestTrampoline() override = default;

   const std::string &Name() const override { return name_; }

   void IssueQuery(const std::vector<mlperf::QuerySample> &samples) override {
      FeedForward(samples);
   }

   void ReportLatencyResults(
         const std::vector<mlperf::QuerySampleLatency> &latencies_ns) override {
      IsMlPerfCompleted = true;
   }

   void FlushQueries() override {}

private:
   std::string name_;
};


class QuerySampleLibraryTrampoline : public mlperf::QuerySampleLibrary {
public:
   QuerySampleLibraryTrampoline(
            std::string name, size_t total_sample_count,
            size_t performance_sample_count)
            : name_(std::move(name)),
            total_sample_count_(total_sample_count),
            performance_sample_count_(performance_sample_count) {}

   ~QuerySampleLibraryTrampoline() override = default;

   const std::string &Name() const override { return name_; }

   size_t TotalSampleCount() override { return total_sample_count_; }

   size_t PerformanceSampleCount() override { return performance_sample_count_; }

   void LoadSamplesToRam(const std::vector<mlperf::QuerySampleIndex> &samples) override {
      printf("Loading samples to Ram %d", (int)performance_sample_count_);
   }
   void UnloadSamplesFromRam(
            const std::vector<mlperf::QuerySampleIndex> &samples) override {
   }

private:
   std::string name_;
   size_t total_sample_count_;
   size_t performance_sample_count_;
};

void PrintErrorStringAndExit() {
   const char* const errStr = zdl::DlSystem::getLastErrorString();
   std::cerr << errStr << std::endl;
   std::exit(EXIT_FAILURE);
}

// Helper for splitting tokenized strings
void split(
   std::vector<std::string>& split_string,
   const std::string& tokenized_string,
   const char separator
)
{
   split_string.clear();
   std::istringstream tokenized_string_stream(tokenized_string);

   while (!tokenized_string_stream.eof())
   {
      std::string value;
      getline(tokenized_string_stream, value, separator);
      if (!value.empty())
      {
         split_string.push_back(value);
      }
   }
}

void cacheInputName(const char* str)
{
   if(!uInputName.empty())
   {
      std::cerr << "No dimensions were specified for input " << uInputName << std::endl;
      std::cerr << "For multiple inputs, specify input name and its dimensions, then repeat it." << std::endl;
      std::exit(EXIT_FAILURE);
   }
   else
   {
      uInputName = str;
   }
}

void parseInputDimensions(const char* str, zdl::DlSystem::TensorShapeMap& tsMap)
{
   // Make sure input name was specified prior to input dimensions.
   if(uInputName.empty())
   {
      std::cerr << "No input name specified for this dimensions" << std::endl;
      std::cerr << "Specify --input_name before --input_dimensions" << std::endl;
      std::exit(EXIT_FAILURE);
   }

   std::vector<std::string> inputs;
   std::string inputNameDimsStr(str);
   split(inputs, inputNameDimsStr, ',');
   zdl::DlSystem::TensorShape ts;
   for( size_t dimIdx=0; dimIdx < inputs.size(); ++dimIdx )
   {
      std::istringstream s(inputs[dimIdx]);
      unsigned int dim;
      s >> dim;
      ts.concatenate(dim);
   }
   tsMap.add(uInputName.c_str(), ts);

   // Clear cache for other inputs
   uInputName.clear();
}

std::vector<std::string> ReadInputList() {
   std::ifstream fileListStream(InputFileListPath);
   if (!fileListStream)
   {
      std::cerr << "Failed to open input file: " << InputFileListPath << "\n";
      std::exit(EXIT_FAILURE);
   }

   std::string fileLine;
   std::vector<std::string> lines;
   while (std::getline(fileListStream, fileLine))
   {
      if (fileLine.empty()) continue;
      lines.push_back(fileLine);
   }
   return lines;
}

size_t calcSizeFromDims(const size_t rank, const zdl::DlSystem::Dimension *dims)
{
   if (rank == 0) return 0;
   size_t size = 1;
   for (size_t i = rank; i > 0; i--)
   {
      if (*dims != 0)
         size *= *dims;
      else
         size *= resizable_dim;
      dims++;
   }
   return size;
}

std::vector<size_t> calcStrides(zdl::DlSystem::TensorShape dims, size_t elementSize){
   std::vector<size_t> strides(dims.rank());
   strides[strides.size() - 1] = elementSize;
   size_t stride = strides[strides.size() - 1];
   for (size_t i = dims.rank() - 1; i > 0; i--)
   {
      if(dims[i] != 0)
         stride *= dims[i];
      else
         stride *= resizable_dim;
      strides[i-1] = stride;
   }
   return strides;
}

bool FloatToTf8(uint8_t* out,
                unsigned char& stepEquivalentTo0,
                float& quantizedStepSize,
                bool dynamicQuantization,
                float* in,
                size_t numElement)
{
   double encodingMin;
   double encodingMax;
   double encodingRange;

   if (dynamicQuantization)
   {
      // Figure out quantizedStepSize and stepEquivalentTo0 based on the
      // specific "in" that was passed into the function.
      float trueMin = std::numeric_limits <float>::max();
      float trueMax = std::numeric_limits <float>::min();

      for (size_t i = 0; i < numElement; ++i) {
         trueMin = fmin(trueMin, in[i]);
         trueMax = fmax(trueMax, in[i]);
      }

      double stepCloseTo0;

      if (trueMin > 0.0f) {
         stepCloseTo0 = 0.0;
         encodingMin = 0.0;
         encodingMax = trueMax;
      } else if (trueMax < 0.0f) {
         stepCloseTo0 = 255.0;
         encodingMin = trueMin;
         encodingMax = 0.0;
      } else {
         double trueStepSize = static_cast <double>(trueMax - trueMin) / 255.0;
         stepCloseTo0 = -trueMin / trueStepSize;
         if (stepCloseTo0==round(stepCloseTo0)) {
            // 0.0 is exactly representable
            encodingMin = trueMin;
            encodingMax = trueMax;
         } else {
            stepCloseTo0 = round(stepCloseTo0);
            encodingMin = (0.0 - stepCloseTo0) * trueStepSize;
            encodingMax = (255.0 - stepCloseTo0) * trueStepSize;
         }
      }

      const double minEncodingRange = 0.01;
      encodingRange = encodingMax - encodingMin;
      quantizedStepSize = encodingRange / 255.0;
      stepEquivalentTo0 = static_cast <unsigned char> (round(stepCloseTo0));

      if (encodingRange < minEncodingRange) {
         std::cerr << "Expect the encoding range to be larger than " <<  minEncodingRange << "\n"
                   << "Got: " << encodingRange << "\n";
         return false;
      }
   }
   else
   {
      // Use the values for quantizedStepSize and stepEquivalentTo0 that were
      // passed into the function, and do not look at "in" to determine these.
      encodingMin = (0 - stepEquivalentTo0) * quantizedStepSize;
      encodingMax = (255 - stepEquivalentTo0) * quantizedStepSize;
      encodingRange = encodingMax - encodingMin;
   }

   for (size_t i = 0; i < numElement; ++i) {
      int quantizedValue = round(255.0 * (in[i] - encodingMin) / encodingRange);

      if (quantizedValue < 0)
         quantizedValue = 0;
      else if (quantizedValue > 255)
         quantizedValue = 255;

      out[i] = static_cast <uint8_t> (quantizedValue);
   }
   return true;
}

void Tf8ToFloat(float *out,
                uint8_t *in,
                const unsigned char stepEquivalentTo0,
                const float quantizedStepSize,
                size_t numElement)
{
   for (size_t i = 0; i < numElement; ++i) {
      double quantizedValue = static_cast <double> (in[i]);
      double stepEqTo0 = static_cast <double> (stepEquivalentTo0);
      out[i] = static_cast <double> ((quantizedValue - stepEqTo0) * quantizedStepSize);
   }
}

void PopulateOutputBufferMapsFloat(zdl::SNPE::SNPE* snpe, zdl::DlSystem::UserBufferMap &outputMap,
                                   std::vector<std::unique_ptr<zdl::DlSystem::IUserBuffer>> &ubs,
                                   std::unordered_map<std::string,std::vector<uint8_t>> &bufs)
{
   zdl::DlSystem::IUserBufferFactory& ubFactory = zdl::SNPE::SNPEFactory::getUserBufferFactory();

   // Output UserBufferMap first
   zdl::DlSystem::Optional<zdl::DlSystem::StringList> outputNamesOpt = snpe->getOutputTensorNames();
   if (outputNamesOpt) {

      auto transformer = [&](const char* name) {
         auto ubaOpt = snpe->getInputOutputBufferAttributes(name);
         size_t bufSize = calcSizeFromDims((*ubaOpt)->getDims().rank(), (*ubaOpt)->getDims().getDimensions()) * sizeof(float);
         bufs.insert(std::pair<std::string, std::vector<uint8_t>>(std::string(name), std::vector<uint8_t>(bufSize)));
         zdl::DlSystem::UserBufferEncodingFloat userBufferEncodingFloat;
         ubs.push_back(ubFactory.createUserBuffer(bufs.at(name).data(),
                                                  bufSize,
                                                  calcStrides((*ubaOpt)->getDims(), sizeof(float)),
                                                  &userBufferEncodingFloat));
         outputMap.add(name, ubs.back().get());
      };
      std::for_each((*outputNamesOpt).begin(), (*outputNamesOpt).end(),
                    transformer);
   }
}

bool LoadInputBufferMapsFloat(std::string& filePaths,
                              const zdl::DlSystem::StringList& inputTensorNames,
                              zdl::SNPE::SNPE* snpe,
                              zdl::DlSystem::UserBufferMap& inputMap,
                              std::vector<std::unique_ptr<zdl::DlSystem::IUserBuffer>> & ubs,
                              std::unordered_map<std::string, std::vector<uint8_t>> &bufs,
                              size_t& numFilesCopied, size_t& batchSize) {
   zdl::DlSystem::IUserBufferFactory& ubFactory = zdl::SNPE::SNPEFactory::getUserBufferFactory();
   for (size_t i = 0; i < inputTensorNames.size(); ++i) {
      const char* name = inputTensorNames.at(i);
      auto ubaOpt = snpe->getInputOutputBufferAttributes(name);
      size_t bufSize = calcSizeFromDims((*ubaOpt)->getDims().rank(), (*ubaOpt)->getDims().getDimensions()) * sizeof(float);

      //Load input buffer data in raw byte format
      std::vector<unsigned char> inputVec;

      if(!LoadBatchedInput(filePaths, bufSize, inputVec, numFilesCopied, batchSize))
      {
          return false;
      }

      if (inputVec.size() != bufSize) {
         std::cerr << "Size of raw input file does not match network. \n"
                   << "Expecting: " << bufSize << "\n"
                   << "Got: " << inputVec.size() << "\n";
         return false;
      }

      bufs.insert(std::pair<std::string, std::vector<uint8_t>>(std::string(name), inputVec));
      zdl::DlSystem::UserBufferEncodingFloat userBufferEncodingFloat;
      ubs.push_back(ubFactory.createUserBuffer(bufs.at(name).data(),
                                               bufSize,
                                               calcStrides((*ubaOpt)->getDims(), sizeof(float)),
                                               &userBufferEncodingFloat));
      inputMap.add(name, ubs.back().get());
   }
   return true;
}

bool LoadInputBufferMapsTf8(std::string& filePaths,
                            const zdl::DlSystem::StringList& inputTensorNames,
                            zdl::SNPE::SNPE* snpe,
                            zdl::DlSystem::UserBufferMap& inputMap,
                            std::vector<std::unique_ptr<zdl::DlSystem::IUserBuffer>> & ubs,
                            std::unordered_map<std::string, std::vector<uint8_t>> &bufs,
                            size_t& numFilesCopied, size_t& batchSize, bool dynamicQuantization) {

   zdl::DlSystem::IUserBufferFactory& ubFactory = zdl::SNPE::SNPEFactory::getUserBufferFactory();
   for (size_t i = 0; i < inputTensorNames.size(); ++i) {
      const char* name = inputTensorNames.at(i);
      auto ubaOpt = snpe->getInputOutputBufferAttributes(name);
      size_t bufSize = calcSizeFromDims((*ubaOpt)->getDims().rank(), (*ubaOpt)->getDims().getDimensions()) * sizeof(uint8_t);
      //Load input buffer data in raw byte format
      std::vector<unsigned char> inputVec;
      if(!LoadBatchedInput(filePaths, bufSize * sizeof(float), inputVec, numFilesCopied, batchSize))
      {
          return false;
      }

      size_t numBytes = inputVec.size();

      if (numBytes % sizeof(float) != 0) {
         std::cerr << "Number of bytes of input file should be divisible by the sizeof (float).\n";
         return false;
      }

      if (numBytes/sizeof(float) != bufSize) {
         std::cerr << "Size of raw input file does not match network. \n"
                   << "Expecting: " << bufSize << "\n"
                   << "Got: " << numBytes/4 << "\n";
         return false;
      }

      std::vector<unsigned char> inputVec_Tf8(numBytes/4);
      float* data_ptr = (float*)(inputVec.data()); // direct casting to float data

      unsigned char stepEquivalentTo0;
      float quantizedStepSize;
      if (!dynamicQuantization)
      {
         const zdl::DlSystem::UserBufferEncodingTf8* ubeTf8 = dynamic_cast<const zdl::DlSystem::UserBufferEncodingTf8*>((*ubaOpt)->getEncoding());
         stepEquivalentTo0 = ubeTf8->getStepExactly0();
         quantizedStepSize = ubeTf8->getQuantizedStepSize();
      }
      if(!FloatToTf8(inputVec_Tf8.data(), stepEquivalentTo0, quantizedStepSize, dynamicQuantization, data_ptr, numBytes/4))
      {
          return false;
      }

      bufs.insert(std::pair<std::string, std::vector<uint8_t>>(std::string(name), inputVec_Tf8));
      zdl::DlSystem::UserBufferEncodingTf8 userBufferEncodingTf8(stepEquivalentTo0,quantizedStepSize);
      ubs.push_back(ubFactory.createUserBuffer(bufs.at(name).data(),
                                               bufSize,
                                               calcStrides((*ubaOpt)->getDims(), sizeof(uint8_t)),
                                               &userBufferEncodingTf8));
      inputMap.add(name, ubs.back().get());
   }
   return true;
}

void PopulateOutputBufferMapsTf8(zdl::SNPE::SNPE* snpe,
                                 zdl::DlSystem::UserBufferMap &outputMap,
                                 std::vector<std::unique_ptr<zdl::DlSystem::IUserBuffer>> &ubs,
                                 std::unordered_map<std::string,
                                 std::vector<uint8_t>> &bufs) {
   zdl::DlSystem::IUserBufferFactory& ubFactory = zdl::SNPE::SNPEFactory::getUserBufferFactory();

   // Output UserBufferMap first
   zdl::DlSystem::Optional<zdl::DlSystem::StringList> outputNamesOpt = snpe->getOutputTensorNames();
   if (outputNamesOpt) {

      auto transformer = [&](const char* name) {
         auto ubaOpt = snpe->getInputOutputBufferAttributes(name);
         size_t bufSize = calcSizeFromDims((*ubaOpt)->getDims().rank(), (*ubaOpt)->getDims().getDimensions());
         bufs.insert(std::pair<std::string, std::vector<uint8_t>>(std::string(name), std::vector<uint8_t>(bufSize)));
         zdl::DlSystem::UserBufferEncodingTf8 userBufferEncodingTf8(0,1.0f);
         ubs.push_back(ubFactory.createUserBuffer(bufs.at(name).data(),
                                                  bufSize,
                                                  calcStrides((*ubaOpt)->getDims(), sizeof(uint8_t)),
                                                  &userBufferEncodingTf8));
         outputMap.add(name, ubs.back().get());
      };
      std::for_each((*outputNamesOpt).begin(), (*outputNamesOpt).end(),
                    transformer);
   }
}

void LoadSamplesToRamInLoop() {
   while(!IsMlPerfCompleted) {
      int size = filePathsList.at(0).size();
      for (int i = 0; i < size && !IsMlPerfCompleted; i++) {
         zdl::DlSystem::UserBufferMap inputMap;
         std::vector<std::unique_ptr<zdl::DlSystem::IUserBuffer>> ubs;
         std::unordered_map<std::string, std::vector<uint8_t>> bufs;

         numFilesCopied = 0;
         batchSize = 0;
         if (usingTf8UserBuffer) {
            if(!LoadInputBufferMapsTf8(filePathsList[0][i], inputTensorNamesList[0], snpe.get(),
                  inputMap, ubs, bufs, numFilesCopied, batchSize, usingDynamicQuantization)) {
               PrintErrorStringAndExit();
            }
         } else  {
            if(!LoadInputBufferMapsFloat(filePathsList[0][i], inputTensorNamesList[0], snpe.get(),
                  inputMap, ubs, bufs, numFilesCopied, batchSize)) {
               PrintErrorStringAndExit();
            }
         }
      }
   }
}

int main(int argc, char **argv) {
   ProcessCommandLine(argc, argv);

   // Open the DL container that contains the network to execute.
   std::unique_ptr<zdl::DlContainer::IDlContainer> container;
   container = zdl::DlContainer::IDlContainer::open(ContainerPath);
   if (!container) {
     PrintErrorStringAndExit();
   }
   // The runtime availability API allows for runtime support to be queried.
   // If a selected runtime is not available, we will issue a warning and continue,
   // expecting the invalid configuration to be caught at SNPE network creation.
   if (!zdl::SNPE::SNPEFactory::isRuntimeAvailable(Runtime)) {
      std::cout << "The selected runtime is not available on this platform. Continue anyway ";
      std::cout << "to observe the failure at network creation time." << std::endl;
   }
   if (Runtime == zdl::DlSystem::Runtime_t::DSP || Runtime == zdl::DlSystem::Runtime_t::AIP_FIXED8_TF) {
      usingTf8UserBuffer = true;
      usingFloatUserBuffer = false;
   } else {
      usingFloatUserBuffer = true;
      usingTf8UserBuffer = false;
   }

   auto lines = ReadInputList();
   zdl::DlSystem::StringList outputLayers;
   if (!DebugNetworkOutputs) {
      outputLayers = ResolveOutputLayerNames(lines);
   }
   if (!lines.empty() && lines.at(0).compare(0, 1, "#") == 0) {
      lines.erase(lines.begin());
   }
   zdl::SNPE::SNPEBuilder snpeBuilder(container.get());
   snpe = snpeBuilder.setOutputLayers(outputLayers)
            .setRuntimeProcessor(Runtime)
            .setDebugMode(DebugNetworkOutputs)
            .setPerformanceProfile(zdl::DlSystem::PerformanceProfile_t::BURST)
            .setProfilingLevel(zdl::DlSystem::ProfilingLevel_t::OFF)
            .setCPUFallbackMode(IsCpuFallbackEnabled)
            .setExecutionPriorityHint(zdl::DlSystem::ExecutionPriorityHint_t::HIGH)
            .setUseUserSuppliedBuffers(usingFloatUserBuffer | usingTf8UserBuffer)
            .setInitCacheMode(usingInitCache)
            .build();
   if(usingInitCache)
   {
      if (container->save(ContainerPath))
      {
         std::cout << "Saved container into archive successfully" << "\n";
      }
      else
      {
         std::cout << "Failed to save container into archive" << "\n";
      }
   }

   std::cout << std::string(79, '-') << "\n";
   std::cout << "Model String: " << snpe->getModelVersion().c_str() << "\n";
   // Print out version information about the model and SNPE library
   std::cout << "SNPE v" << zdl::SNPE::SNPEFactory::getLibraryVersion().toString() << "\n";
   std::cout << std::string(79, '-') << "\n";

   const auto &strList_opt = snpe->getInputTensorNames();
   if (!strList_opt)
      throw std::runtime_error("Error obtaining Input tensor names");
   const auto &networkInputTensorNames = *strList_opt;

   // Read the input list
   size_t num = 0;

   for (auto& line : lines) {
      auto namesPathsPair = ParseInputLine(line);

      zdl::DlSystem::StringList inputTensorNames;
      std::vector<std::string> &filePaths = namesPathsPair.second;
      std::set<std::string> iterationInputLayerNames;

      if (namesPathsPair.first.size() > 0) {
         for (auto& name : namesPathsPair.first) {
            inputTensorNames.append(name.c_str());
            iterationInputLayerNames.insert(name);
         }

         if (iterationInputLayerNames.size() != namesPathsPair.first.size()) {
            std::cerr << "Input names have duplicate at input line " << num + 1 << "\n";
            std::exit(EXIT_FAILURE);
         }
      }

      if (inputTensorNames.size() == 0)
         inputTensorNames = networkInputTensorNames;
      else {
         for(const auto& name : networkInputTensorNames) {
            if (iterationInputLayerNames.find(std::string(name)) == iterationInputLayerNames.end()) {
               std::cerr << "Name of inputs in the file do not match the name of"
                         << " inputs to the network.\n"
                         << "Missing " << name << " at input line "
                         << num + 1 << "\n";
               std::exit(EXIT_FAILURE);
            }
         }
      }

      if (filePaths.size() != inputTensorNames.size()) {
         std::cerr << "Number of input files does not match the number of"
                   << " inputs to the network.\n"
                   << "Expecting: " << inputTensorNames.size() << "\n"
                   << "Got: " << filePaths.size() << "\n";
         std::exit(EXIT_FAILURE);
      }

      inputTensorNamesList.push_back(inputTensorNames);
      for (size_t i = 0; i < filePaths.size(); i++) {
         if (i >= filePathsList.size())
           filePathsList.push_back(std::vector<std::string>());

         filePathsList[i].push_back(filePaths[i]);
      }
      num++;
   }

   if (IsAccuracyMode) {
      for (int i = 0, n = filePathsList.at(0).size(); i < n; i++) {
         LoadSample(i);
         FeedForwardUserBuffer(i);
         SaveOutputBuffer(i);

         // Clearing all the buffers
         outputMaps.at(i).clear();
         inputMaps.at(i).clear();
         bufs_list.at(i).clear();
         ubs_list.at(i).clear();
         ubs_list.at(i).shrink_to_fit();
      }
   } else {
      // Load all the images to Ram
      LoadSamplesToRam();
      // Warm up few samples
      std::cout << "Warming up few samples" << endl;
      int size = filePathsList.at(0).size();
      for (int p = 0, n = std::min(10, size); p < n; p++) {
         //LoadSample(p);
         FeedForwardUserBuffer(p);
      }
      std::cout << "Warming up finished" << endl;
      // This is to keep CPU busy
      std::thread loadSamples(LoadSamplesToRamInLoop);

      // ML Perf test settings
      SystemUnderTestTrampoline* sut = new SystemUnderTestTrampoline();
      QuerySampleLibraryTrampoline* qsl = new QuerySampleLibraryTrampoline("Query Sample library test",
         size, std::min(size, 1024));
      mlperf::LogSettings default_log_settings;
      mlperf::TestSettings testSettings;
      default_log_settings.enable_trace = false;
      testSettings.FromConfig(MLPerfConfig, ModelName, MLPerfTestScenario);
      testSettings.scenario = mlperf::TestScenario::SingleStream;
      testSettings.single_stream_expected_latency_ns = SingleStreamMaxLatency;
      mlperf::StartTest(sut, qsl, testSettings, default_log_settings);
      delete sut;
      delete qsl;
      loadSamples.join();
   }
   return 0;
}

void LoadSample(int index) {
   zdl::DlSystem::UserBufferMap inputMap, outputMap;
   std::vector<std::unique_ptr<zdl::DlSystem::IUserBuffer>> ubs;
   std::unordered_map<std::string, std::vector<uint8_t>> bufs;

   numFilesCopied = 0;
   batchSize = 0;
   if (usingTf8UserBuffer) {
      if(!LoadInputBufferMapsTf8(filePathsList[0][index], inputTensorNamesList[0], snpe.get(),
            inputMap, ubs, bufs, numFilesCopied, batchSize, usingDynamicQuantization)) {
         PrintErrorStringAndExit();
      }
      PopulateOutputBufferMapsTf8(snpe.get(), outputMap, ubs, bufs);
   } else  {
      if(!LoadInputBufferMapsFloat(filePathsList[0][index], inputTensorNamesList[0], snpe.get(),
            inputMap, ubs, bufs, numFilesCopied, batchSize)) {
         PrintErrorStringAndExit();
      }
      PopulateOutputBufferMapsFloat(snpe.get(), outputMap, ubs, bufs);
   }

   ubs_list.push_back(std::move(ubs));
   bufs_list.push_back(std::move(bufs));
   inputMaps.push_back(std::move(inputMap));
   outputMaps.push_back(std::move(outputMap));
}

void LoadSamplesToRam() {
   int size = filePathsList.at(0).size();
   for (int i = 0; i < size; i++) {
      LoadSample(i);
   }
}

void FeedForwardUserBuffer(size_t index) {
   if(!snpe->execute(inputMaps.at(index), outputMaps.at(index))) {
      PrintErrorStringAndExit();
   }
}

void FeedForward(const std::vector<mlperf::QuerySample> &samples) {
   std::vector<mlperf::QuerySampleResponse> responses;
   int count = 0;
   while (!filePathsList.at(0).empty() && count < samples.size()) {
      FeedForwardUserBuffer(samples[count].index);
      mlperf::QuerySampleResponse response;
      response.id = samples[count].id;
      responses.push_back(response);
      count++;
   }
   mlperf::QuerySamplesComplete(responses.data(), responses.size());
}

void SaveOutputBuffer(int index) {
   zdl::DlSystem::Optional<zdl::DlSystem::StringList> outputNamesOpt = outputMaps.at(index).getUserBufferNames();
   if (outputNamesOpt) {
      const auto &outputNamesList = *outputNamesOpt;
      for(const char *name: outputNamesList) {
          for(size_t j = 0; j < numFilesCopied; j++) {
              std::ostringstream path;
              path << OutputDir << "/"
                  << "Result_" << index + j << "/"
                  << name << ".raw";

              auto bufSize = outputMaps.at(index).getUserBuffer(name)->getSize();
              auto dataSize = outputMaps.at(index).getUserBuffer(name)->getOutputSize();
              const zdl::DlSystem::UserBufferEncoding& ubEncoding = outputMaps.at(index).getUserBuffer(name)->getEncoding();
              const zdl::DlSystem::UserBufferEncodingFloat* ubEncodingFloat
                      = dynamic_cast <const zdl::DlSystem::UserBufferEncodingFloat*> (&ubEncoding);
              size_t tensorStart = j * bufSize / batchSize;
              size_t tensorSize =  bufSize / batchSize;
              if (ubEncodingFloat!=nullptr) {
                  if(bufSize != dataSize) {
                      std::cout << "UserBuffer size is " << bufSize << " bytes, but "
                                << dataSize << " bytes of data was found." << std::endl;
                      if( dataSize > bufSize )
                          std::cout << "Assign a larger buffer using a bigger --resizable_dim argument" << std::endl;
                      tensorStart = j * std::min(bufSize,dataSize) / batchSize;
                      tensorSize = std::min(bufSize,dataSize) / batchSize;
                  }

                  if(!SaveUserBuffer(path.str(), bufs_list.at(index).at(name).data() + tensorStart, tensorSize)) {
                      PrintErrorStringAndExit();
                  }
              }
              else {
                  const zdl::DlSystem::UserBufferEncodingTf8* ubEncodingTf8
                      = dynamic_cast <const zdl::DlSystem::UserBufferEncodingTf8*> (&ubEncoding);
                  if (ubEncodingTf8!=nullptr) {
                      size_t numElement = bufSize / sizeof(uint8_t);
                      if (bufSize != dataSize) {
                          std::cout << "UserBuffer size is " << bufSize << " bytes, but "
                                    << dataSize << " bytes of data was found." << std::endl;
                          if (dataSize > bufSize)
                              std::cout << "Assign a larger buffer using a bigger --resizable_dim argument"
                                        << std::endl;
                          tensorStart = j * std::min(bufSize, dataSize) / batchSize;
                          numElement = std::min(bufSize, dataSize) / sizeof(uint8_t);
                      }

                      std::vector<float> outputVector(numElement);
                      // external dequantizer
                      const unsigned char stepEquivalentTo0 = ubEncodingTf8->getStepExactly0();
                      const float quantizedStepSize = ubEncodingTf8->getQuantizedStepSize();
                      Tf8ToFloat(outputVector.data(), bufs_list.at(index).at(name).data(), stepEquivalentTo0,
                                 quantizedStepSize, numElement);
                      const unsigned char *dataPtr = (const unsigned char *) (outputVector.data());
                      if (!SaveUserBuffer(path.str(), dataPtr + tensorStart * sizeof(float),
                                          numElement * sizeof(float) / batchSize)) {
                          PrintErrorStringAndExit();
                      }
                  }
                  else {
                      std::cerr << "Invalid output tensor type." << "\n";
                      PrintErrorStringAndExit();
                  }
              }
          }
      }
   } else {
      std::cerr << "Cannot obtain the names of output UserBuffer." << "\n";
      PrintErrorStringAndExit();
   }
}

std::pair<std::vector<std::string>, std::vector<std::string>> ParseInputLine(std::string &line) {
   std::vector<std::string> names {};
   std::vector<std::string> paths {};
   std::string separator = ":=";

   std::vector<std::string> inputsInfo;
   split(inputsInfo, line, ' ');
   for (auto& inputInfo : inputsInfo) {
      auto position = inputInfo.find(separator);
      if (position != std::string::npos) {
         auto name = inputInfo.substr(0, position);
         names.push_back(name);
         auto path = inputInfo.substr(position + separator.size());
         paths.push_back(path);
      } else {
         paths.push_back(inputInfo);
      }
   }
   return {names, paths};
}

static bool
SaveUserBuffer(const std::string& path, const unsigned char* buffer, size_t size)
{
   // Create the directory path if it does not exist
   auto idx = path.find_last_of('/');
   if (idx != std::string::npos) {
      std::string dir = path.substr(0, idx);
      if (!EnsureDirectoryExists(dir)) {
         std::cerr << "Failed to create output directory: " << dir << ": "
                   << std::strerror(errno) << "\n";
         return false;
      }
   }

   std::ofstream os(path, std::ofstream::binary);
   if (!os) {
      std::cerr << "Failed to open output file for writing: " << path << "\n";
      return false;
   }

   if(!os.write((const char*)buffer, size)) {
      std::cerr << "Failed to write data to: " << path << "\n";
      return false;
   }

   return true;
}

zdl::DlSystem::StringList ResolveOutputLayerNames(std::vector<std::string> &lines) {
   zdl::DlSystem::StringList outputLayers;
   if (!lines.empty() && lines.at(0).compare(0, 1, "#") == 0) {
      std::vector<std::string> names;
      split(names, lines.at(0).substr(1), ' ');
      for (auto& name : names)
         outputLayers.append(name.c_str());
   }
   return outputLayers;
}

template <typename Vector>
std::vector<Vector> LoadInputFile(const std::string& inputFile) {
   std::ifstream in(inputFile, std::ifstream::binary);
   if (!in.is_open() || !in.good()) {
      std::cerr << "Failed to open input file: " << inputFile << "\n";
      std::exit(EXIT_FAILURE);
   }

   in.seekg(0, in.end);
   int length = in.tellg();
   in.seekg(0, in.beg);

   if (length % sizeof(Vector) != 0) {
       std::cerr << "Size of input file should be divisible by sizeof(dtype).\n";
       std::exit(EXIT_FAILURE);
   }

   std::vector<Vector> vec;
   vec.resize(length/sizeof(Vector));
   if (!in.read(reinterpret_cast<char*>(&vec[0]), length)) {
      std::cerr << "Failed to read the contents of: " << inputFile << "\n";
      std::exit(EXIT_FAILURE);
   }

   return vec;
}

template <typename Vector>
static bool LoadBatchedInput(std::string& filePathsQueue, const size_t& inputSize,
                             std::vector<Vector>& inputVec, size_t& numFilesCopied,
                             size_t& batchSize) {
   size_t inputRawSize = 0;
   std::string combinedFileNames;
   size_t numInputsCopied = 0;
   size_t numBatchSize = 0;
   do {
      if (!filePathsQueue.empty()) {
          std::vector<Vector> loadedFile = LoadInputFile<Vector>(
                  filePathsQueue);
          combinedFileNames += filePathsQueue + "\n";

          if (!inputRawSize)
              inputRawSize = loadedFile.size();

          if (((inputSize % loadedFile.size()) != 0) || (loadedFile.size() > inputSize) ||
              (loadedFile.size() == 0) || (inputRawSize != loadedFile.size())) {
              std::cerr << "Error: invalid input size provided. Please check all raw sizes." << "\n";
              return false;
          }

          inputVec.insert(inputVec.end(), loadedFile.begin(), loadedFile.end());
          numInputsCopied += 1;
      }
      else {
          // pad the vector with zeros
          combinedFileNames += "Applying padding\n";
          std::vector<Vector> emptyVector(inputRawSize, 0);
          inputVec.insert(inputVec.end(), emptyVector.begin(), emptyVector.end());
      }

       numBatchSize += 1;
   } while(inputVec.size() < inputSize);

   // std::cout << "Processing DNN input(s): " << combinedFileNames << endl;
   numFilesCopied = numInputsCopied;
   batchSize = numBatchSize;
   return true;
}

void ProcessCommandLine(int argc, char** argv)
{
   enum OPTIONS
   {
      OPT_HELP          = 0,
      OPT_CONTAINER     = 1,
      OPT_INPUT_LIST    = 2,
      OPT_OUTPUT_DIR    = 3,
      OPT_STORAGE_DIR   = 4,
      OPT_DEBUG_OUTPUTS = 5,
      OPT_USE_GPU       = 6,
      OPT_ENCODING_TYPE = 7,
      OPT_USE_DSP       = 8,
      OPT_VERSION       = 10,
      OPT_ENABLE_CPU_FALLBACK = 11,
      OPT_INPUT_NAME    = 14,
      OPT_INPUT_DIMENSIONS = 15,
      OPT_PLATFORM_OPTIONS = 18,
      OPT_RESIZABLE_DIM = 20,
      OPT_PROFILING_LEVEL = 21,
      OPT_ACCURACY = 25,
      OPT_USE_AIP       = 101,
      OPT_MODEL_NAME    = 102,
      OPT_MLPERF_CONFIG = 103,
      OPT_MAX_LATENCY   = 104
   };

   // Create the command line options
   static struct option long_options[] = {
      {"help",                    no_argument,          NULL,  OPT_HELP },
      {"accuracy",                no_argument,          NULL,  OPT_ACCURACY },
      {"container",               required_argument,    NULL,  OPT_CONTAINER },
      {"input_list",              required_argument,    NULL,  OPT_INPUT_LIST },
      {"output_dir",              required_argument,    NULL,  OPT_OUTPUT_DIR },
      {"storage_dir",             required_argument,    NULL,  OPT_STORAGE_DIR },
      {"debug",                   no_argument,          NULL,  OPT_DEBUG_OUTPUTS },
      {"use_gpu",                 no_argument,          NULL,  OPT_USE_GPU },
      {"encoding_type",           required_argument,    NULL,  OPT_ENCODING_TYPE },
      {"use_dsp",                 no_argument,          NULL,  OPT_USE_DSP },
      {"use_aip",                 no_argument,          NULL,  OPT_USE_AIP },
      {"version",                 no_argument,          NULL,  OPT_VERSION },
      {"enable_cpu_fallback",     no_argument,          NULL,  OPT_ENABLE_CPU_FALLBACK},
      {"input_name",              required_argument,    NULL,  OPT_INPUT_NAME},
      {"input_dimensions",        required_argument,    NULL,  OPT_INPUT_DIMENSIONS},
      {"platform_options",        required_argument,    NULL,  OPT_PLATFORM_OPTIONS},
      {"profiling_level",         required_argument,    NULL,  OPT_PROFILING_LEVEL},
      {"model_name",              required_argument,    NULL,  OPT_MODEL_NAME},
      {"mlperf_config",           required_argument,    NULL,  OPT_MLPERF_CONFIG},
      {"max_latency",             required_argument,    NULL,  OPT_MAX_LATENCY},
      {NULL,                      0,                    NULL,  0 }
   };

   // Command line parsing loop
   int long_index =0;
   int opt= 0;
   while ((opt = getopt_long_only(argc, argv, "", long_options, &long_index )) != -1) {
      switch (opt) {
         case OPT_MODEL_NAME:
            ModelName = optarg;
            break;

         case OPT_MLPERF_CONFIG:
            MLPerfConfig = optarg;
            break;

         case OPT_ACCURACY:
            IsAccuracyMode = true;
            break;

         case OPT_MAX_LATENCY:
            // Single Stream max latency in nano seconds
            SingleStreamMaxLatency = stof(std::string(optarg)) * 1000000;
            break;

         case OPT_HELP:
            ShowHelp();
            std::exit(0);
            break;

         case OPT_CONTAINER:
            ContainerPath = optarg;
            break;

         case OPT_INPUT_LIST:
            InputFileListPath = optarg;
            break;

         case OPT_DEBUG_OUTPUTS:
            DebugNetworkOutputs = true;
            break;

         case OPT_OUTPUT_DIR:
            OutputDir = optarg;
            break;

         case OPT_STORAGE_DIR:
            StorageDir = optarg;
            break;

         case OPT_USE_GPU:
            Runtime = zdl::DlSystem::Runtime_t::GPU;
            break;
         case OPT_USE_DSP:
            Runtime = zdl::DlSystem::Runtime_t::DSP;
            break;
         case OPT_USE_AIP:
            Runtime = zdl::DlSystem::Runtime_t::AIP_FIXED8_TF;
            break;
         case OPT_ENCODING_TYPE:
            if (std::strcmp("nv21", optarg) == 0) {
               isInputNv21EncodingType = true;
            }
            else
            {
                std::cerr<< "ERROR: Invalid setting passed to the argument "<< argv[optind-2] << " : " << argv[optind-1] << "\nPlease check the Arguments section in the description below.\n";
                ShowHelp();
                std::exit(EXIT_FAILURE);
            }
            break;
         case OPT_VERSION:
            std::cout << "SNPE v" << zdl::SNPE::SNPEFactory::getLibraryVersion().toString() << std::endl;
            std::exit(0);
         case OPT_ENABLE_CPU_FALLBACK:
            IsCpuFallbackEnabled = true;
            break;
         case OPT_INPUT_NAME:
            cacheInputName(optarg);
            break;
         case OPT_INPUT_DIMENSIONS:
            parseInputDimensions(optarg, InputDimensionsMap);
            break;
         case OPT_PLATFORM_OPTIONS:
            PlatformOptions = optarg;
            break;
         case OPT_PROFILING_LEVEL:
            if (std::strcmp("basic", optarg) == 0)
            {
               ProfilingLevel = zdl::DlSystem::ProfilingLevel_t::BASIC;
            }
            else if (std::strcmp("off", optarg) == 0)
            {
               ProfilingLevel = zdl::DlSystem::ProfilingLevel_t::OFF;
            }
            else if (std::strcmp("detailed", optarg) != 0)
            {
                std::cerr<< "ERROR: Invalid setting passed to the argument "<< argv[optind-2] << " : " << argv[optind-1] << "\nPlease check the Arguments section in the description below.\n";
                ShowHelp();
                std::exit(EXIT_FAILURE);
            }
            break;
         default:
            std::cerr << "ERROR: Invalid argument passed: " << argv[optind-1] << "\nPlease check the Arguments section in the description below.\n";
            ShowHelp();
            std::exit(EXIT_FAILURE);
      }
   }

   if (ContainerPath.empty()) {
      std::cerr << "Missing option: --container\n";
      ShowHelp();
      std::exit(EXIT_FAILURE);
   }

   if (InputFileListPath.empty()) {
      std::cerr << "Missing option: --input_list\n";
      ShowHelp();
      std::exit(EXIT_FAILURE);
   }
}

void ShowHelp(void)
{
   std::cout
      << "\nDESCRIPTION:\n"
      << "------------\n"
      << "Example application demonstrating how to load and execute a neural network\n"
      << "using the SNPE C++ API.\n"
      << "\n\n"
      << "REQUIRED ARGUMENTS:\n"
      << "-------------------\n"
      << "  --container  <FILE>   Path to the DL container containing the network.\n"
      << "  --input_list <FILE>   Path to a file listing the inputs for the network.\n"
      << "                        Optionally the file can have \"#\" starting line to specify the layer names for which output tensor files are to be produced.\n"
      << "                        For more details about the input_list file format, please refer to SDK html documentation (doc/html/tools.html#tools_snpe-net-run_input_list)\n"
      << "\n\n"
      << "OPTIONAL ARGUMENTS:\n"
      << "-------------------\n"
      << "  --use_gpu             Use the GPU runtime for SNPE.\n"
      << "  --use_dsp             Use the DSP fixed point runtime for SNPE.\n"
      << "  --use_aip             Use the AIP fixed point runtime for SNPE.\n"
      << "  --debug                               Specifies that output from all layers of the network\n"
      << "                                        will be saved.\n"
      << "  --output_dir <DIR>                    The directory to save output to. Defaults to ./output\n"
      << "  --storage_dir <DIR>                   The directory to store SNPE metadata files \n"
      << "  --encoding_type <VAL>                 Specifies the encoding type of input file. Valid settings are \"nv21\". \n"
      << "                                        Cannot be combined with --userbuffer*.\n"
      << "  --profiling_level <VAL>               Specifies the profiling level.  Valid settings are \"off\", \"basic\" and \"detailed\".\n"
      << "                                        Default is detailed.\n"
      << "                                        Basic profiling only applies to CPU, DSP, AIP runtimes.\n"
      << "  --enable_cpu_fallback                 Enables cpu fallback functionality. Defaults to disable mode. \n"
      << "  --input_name <INPUT_NAME>             Specifies the name of input for which dimensions are specified.\n"
      << "  --input_dimensions <INPUT_DIM>        Specifies new dimensions for input whose name is specified in input_name. e.g. \"1,224,224,3\".\n"
      << "                                        For multiple inputs, specify --input_name and --input_dimensions multiple times. \n"
      << "  --platform_options <VAL>              Specifies value to pass as platform options.\n"
      << "  --help                                Show this help message.\n"
      << "  --version                             Show SNPE Version Number.\n"
      << "  --accuracy                            Selects accuracy over performance.\n"
      << "  --model_name                          Model name on which TestSettings has to be applied.\n"
      << "  --mlperf_config                       mlperf_conf file to set tune the TestSettings.\n"
      << "  --max_latency                         mlperf_conf TestSettings single stream max latency parameter in milliseconds.\n"
      << std::endl;
}

bool EnsureDirectoryExists(const std::string& dir)
{
   auto i = dir.find_last_of('/');
   std::string prefix = dir.substr(0, i);

   if (dir.empty() || dir == "." || dir == "..") {
      return true;
   }

   if (i != std::string::npos && !EnsureDirectoryExists(prefix)) {
      return false;
   }

   int rc = mkdir(dir.c_str(),  S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
   if (rc == -1 && errno != EEXIST) {
      return false;
   }
   else {
      struct stat st;
      if (stat(dir.c_str(), &st) == -1) {
         return false;
      }

      return S_ISDIR(st.st_mode);
   }
}
