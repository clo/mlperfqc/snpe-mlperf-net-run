# Copyright (c) 2019 Qualcomm Innovation Center, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name Qualcomm Innovation Center nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import logging
import sys
from subprocess import check_output, STDOUT
from functools import wraps

import datetime
import json
import logging
import os
import re
import time
import traceback
import multiprocessing as mp
import argparse

from .common_utils import *

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("adb")
FNULL = open(os.devnull, 'w')
REGX_GET_PROP = re.compile('\[(.+)\]: \[(.+)\]')
getprop_list = ['ro.product.name',
                'ro.serialno',
                'ro.product.model',
                'ro.product.board',
                'ro.product.brand',
                'ro.product.device',
                'ro.product.manufacturer',
                'ro.product.cpu.abi',
                'ro.build.au_rev',
                'ro.build.description',
                'ro.build.version.sdk']
UNKNOWN = 'unknown'

# Path where the images need to be pushed
device_images_path = ''


def retry(tries=3, delay=1, backoff=2):
    def retry_decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            l_tries, l_delay = tries, delay
            while l_tries > 0:
                logger.debug('Try {}'.format(tries - l_tries))
                code, out, err = func(*args, **kwargs)
                if code == 0:
                    return code, out, err
                logger.debug('Failed. Retrying')
                l_tries -= 1
                l_delay *= backoff
                time.sleep(l_delay)
            return func(*args, **kwargs)
        return wrapper
    return retry_decorator


class Process(mp.Process):
    def __init__(self, *args, **kwargs):
        mp.Process.__init__(self, *args, **kwargs)
        self._pconn, self._cconn = mp.Pipe()
        self._exception = None

    def run(self):
        try:
            mp.Process.run(self)
            self._cconn.send(None)
        except Exception as e:
            tb = traceback.format_exc()
            self._cconn.send((e, tb))

    @property
    def exception(self):
        if self._pconn.poll():
            self._exception = self._pconn.recv()
        return self._exception


def execute_cmd(cmd_str):
    logger.debug('Executing {%s}' % cmd_str)
    try:
        cmd_handle = check_output(cmd_str, stderr=STDOUT, shell=True)
        logger.debug('Command Output: \n"%s"' % cmd_handle)
        return cmd_handle
    except Exception as e:
        raise e

class AdbShellCmdFailedException(Exception):
    def __str__(self):
        return('\nadb shell command Error: ' + repr(self) + '\n')


class Adb(object):
    def __init__(self, adb_executable, device, master_id=None, hostname='localhost'):
        if not os.path.exists(adb_executable):
            exists = False
            for path in os.environ['PATH'].split(os.pathsep):
                if os.path.exists(os.path.join(path, 'adb')):
                    exists = True
                    break
            if not exists:
                logger.error('Invalid path for adb: %s.' % adb_executable)
                raise RuntimeError('adb not in PATH')
        self.__adb_executable = adb_executable
        self.__master_id = master_id
        self._adb_device = device
        self._hostname = 'localhost' if hostname is None else hostname

    def pull_files(self, src_list, dest=os.getcwd()):
        for src in src_list:
            self.pull(src, dest)

    @retry()
    def push(self, src, dst, cwd='.'):
        dst_dir_exists = False
        if (self._execute('shell', ['[ -d %s ]' % dst], cwd=cwd)[0] == 0):
            dst_dir_exists = True
        else:
            if (os.path.basename(src) == os.path.basename(dst)) and not os.path.isdir(src):
                dir_name = os.path.dirname(dst)
            else:
                dir_name = dst
            ret, _, err = self._execute('shell', ['mkdir', '-p', dir_name])
            if ret != 0:
                logger.warning('mkdir failed for parent folder')
        code, out, err = self._execute('push', [src, dst], cwd=cwd)
        if code == 0:
            # Check if push was successful
            if src[-1] == '/':
                src = src[:-1]
            file_name = src.split('/')[-1]
            # check if destination directory exists
            # if it exists, then append file name to dst
            # otherwise, adb will rename src dir to dst
            if dst_dir_exists:
                dst = (dst + file_name) if dst[-1] == '/' else (dst + '/' + file_name)
            code, out, error = self._execute('shell',['[ -e %s ]' % dst], cwd=cwd)
        return code, out, err

    @retry()
    def pull(self, src, dst, cwd='.'):
        return self._execute('pull', [src, dst], cwd=cwd)

    def shell(self, command, args=[]):
        shell_args = ['{} {}'.format(command, ' '.join(args))]
        code, out, err = self._execute('shell', shell_args)
        if code == 0:
            if len(out) > 0:
                try:
                    code = int(out[-1])
                    out = out[:-1]
                except ValueError as ex:
                    code = -1
                    out.append(str(ex))
            else:
                code = -1

            if code != 0 and len(err) == 0:
                err = out
        else:
            code = -1
        return code, out, err

    def _execute(self, command, args, cwd='.', recovery_enable=True):
        if self._adb_device is None or len(self._adb_device) == 0:
            adb_command_args = ['-H', self._hostname, command] + args
        else:
            adb_command_args = ['-H', self._hostname, '-s', self._adb_device,
                                command] + args
        (return_code, output, error) = execute(self.__adb_executable, adb_command_args, cwd=cwd, timeout=Timeouts.ADB_DEFAULT_TIMEOUT)
        # when the process gets killed, it will return -9 code; Logging this error for debug purpose
        if return_code == -9:
            logger.error("adb command didn't execute within the timeout. Is device in good state?")

        if self._adb_device:
            issue = ""
            if "error: device offline" in error:
                issue = "offline"
            elif "error: device \'{0}\' not found".format(self._adb_device) in error or "error: no devices/emulators found" in error or return_code == -9:
                issue = "crash"

            if recovery_enable and issue != "":
                device_state = self.recover_device(issue=issue)

                if device_state:
                    # Adding info log to print the last failed command
                    logger.info("Retrying the command after device recovery: {0} {1}".format(self.__adb_executable, adb_command_args))
                    (return_code, output, error) = execute(self.__adb_executable, adb_command_args, cwd=cwd, timeout=Timeouts.ADB_DEFAULT_TIMEOUT)
                else:
                    raise AdbShellCmdFailedException("Device not recovered from the bad state. Exiting Job")

        return return_code, output, error

    @retry()
    def get_devices(self):
        code, out, err = self._execute('devices', [])
        if code != 0:
            logger.error("Could not retrieve list of adb devices connected, following error "
                         "occured: {0}".format("\n".join(err)))
            return code, out, err

        devices = []
        for line in out:
            # Checking the connected adb devices with serial id and ip address
            match_obj = re.match("^([a-zA-Z0-9.]+(:[0-9]+)?)\s+device", line, re.M)
            if match_obj:
                devices.append(match_obj.group(1))
        return code, devices, err

    @retry()
    def get_device_info(self, fatal=True):
        _info = {}
        ret, out, err = self._execute('shell', ['getprop'])
        if ret != 0:
            if fatal != True:
                logger.warning('Non fatal get prop call failure, is the target os not Android?')
                return ret, [], err
        if out:
            for line in out:
                line = line.strip()
                m = REGX_GET_PROP.search(line)
                if m:
                    _info[m.group(1)] = m.group(2)
        dev_info = []
        for prop_key in getprop_list:
            if not prop_key in _info:
                dev_info.append([prop_key, UNKNOWN])
            else:
                dev_info.append([prop_key, _info[prop_key]])
        return ret, dev_info, err

    def check_file_exists(self, file_path):
        """
        Returns 'True' if the file exists on the target
        Using 'ls' instead of 'test' cmd as 'test' was behaving abnormally on LE
        """
        ret, out, err = self._execute('shell', ['ls', file_path, '| wc -l'])
        if 'No such file or directory' in ''.join(err) or 'No such file or directory' in ''.join(out):
            ret = 1
        return ret == 0

    def push_file_img(self, img):
        self.push(img, device_images_path)


def main():
    parser = argparse.ArgumentParser(description="Batch convert jpgs",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--dest', type=str, required=True)
    parser.add_argument('-i', '--img_folder', type=str, required=True)

    args = parser.parse_args()
    global device_images_path
    device_images_path = args.dest
    imgs = []
    for root,dirs,files in os.walk(args.img_folder):
        for jpgs in files:
            src_image=os.path.join(root, jpgs)
            if '.raw' in src_image:
                imgs.append(src_image)
    adb = Adb("adb", "")
    pool = mp.Pool(mp.cpu_count())
    pool.map(adb.push_file_img, imgs)


if __name__ == '__main__':
    main()
