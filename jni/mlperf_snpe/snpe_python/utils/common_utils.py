# Copyright (c) 2019 Qualcomm Innovation Center, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name Qualcomm Innovation Center nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import subprocess
import sys
import logging
import os
from threading import Timer
from shutil import copyfile
from subprocess import check_output, STDOUT

logger = logging.getLogger(__name__)


class Timeouts:
    DEFAULT_POPEN_TIMEOUT = 9223372035
    ADB_DEFAULT_TIMEOUT = 9223372035


def __format_output(output):
    """
    Separate lines in output into a list and strip each line.
    :param output: str
    :return: []
    """
    stripped_out = []
    if output is not None and len(output) > 0:
        stripped_out = [line.strip() for line in output.split('\n') if line.strip()]
    return stripped_out


def execute(command, args=[], cwd='.', shell=False, timeout=Timeouts.DEFAULT_POPEN_TIMEOUT):
    """
    Execute command in cwd.
    :param command: str
    :param args: []
    :param cwd: filepath
    :param shell: True/False
    :param timeout: float
    :return: int, [], []
    """
    try:
        logger.debug("Host Command: {} {}".format(command, args))
        process = subprocess.Popen([command] + args,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   cwd=cwd,
                                   shell=shell)
        try:
            # timer is created to kill the process after the timeout
            timer = Timer(float(timeout), process.kill)
            timer.start()
            output, error = process.communicate()
            if sys.version_info[0] == 3:
                output = output.decode('utf-8')
                error = error.decode('utf-8')
        finally:
            # If the timer is alive, that implies process exited within the timeout;
            # Hence stopping the timer task;
            if timer.isAlive():
                timer.cancel()
            else:
                logger.error("Timer expired for the process. Process didn't \
                              finish within the given timeout of %f" % (timeout))

        return_code = process.returncode
        logger.debug("Result Code (%d): stdout: (%s) stderr: (%s)" % (return_code, output, error))
        return return_code, __format_output(output), __format_output(error)
    except OSError as error:
        return -1, [], __format_output(str(error))


def execute_cmd(cmd_str):
    logger.debug('Executing {%s}' % cmd_str)
    try:
        cmd_handle = check_output(cmd_str, stderr=STDOUT, shell=True)
        logger.debug('Command Output: \n"%s"' % cmd_handle)
        return cmd_handle
    except Exception as e:
        raise e