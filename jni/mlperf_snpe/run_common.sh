#!/bin/bash
# Copyright (c) 2019 Qualcomm Innovation Center, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name Qualcomm Innovation Center nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if [ $# -lt 1 ]; then
    echo "usage: $0 tf|onnxruntime|pytorch|tflite|snpe [resnet50|mobilenet|ssd-mobilenet|ssd-resnet34] [cpu|gpu|dsp|aip]"
    exit 1
fi
if [ "x$DATA_DIR" == "x" ]; then
    echo "DATA_DIR not set" && exit 1
fi
if [ "x$MODEL_DIR" == "x" ]; then
    echo "MODEL_DIR not set" && exit 1
fi

## Check if there are more than one device connected ?
if [ "x$DEVICE_ID" == "x" ]; then
   device_list=( $(adb devices | grep -w "device" | awk '{print $1}') )
   if [ ${#device_list[@]} -eq 0 ]; then
      echo 'No adb devices connected, please connect to the host' && exit 1
   elif [ ${#device_list[@]} -gt 1 ]; then
      echo "More than one device connected."
      echo "Add one of the device to EXTRA_OPS as --device <<device_id>>"
      i=1;
      for choice in ${device_list[@]}
      do
         echo "     $i. $choice"
         i=$(($i+1))
      done
      exit 1
   fi
fi

# defaults
backend=snpe
model=mobilenet
runtime="cpu"
model_type="classifier"

for i in $* ; do
    case $i in
       tf|onnxruntime|tflite|pytorch|snpe) backend=$i; shift;;
       resnet50|mobilenet|ssd-mobilenet|ssd-resnet34) model=$i; shift;;
       gpu|dsp|cpu|aip) runtime=$i; shift;;
    esac
done

name="$model-$backend"
extra_args=""
export PYTHONPATH="${PYTHONPATH}:$(pwd)/../../external/mlperf_inference/v0.5/classification_and_detection/python"
export PATH_SDCARD=/storage/mlperf/imagenet_mobilenet_snpe

#
# snpe
#
if [ $name == "resnet50-snpe" ] ; then
    model_path="$MODEL_DIR/resnet50_v1.dlc"
    profile=resnet50-snpe
    export PATH_SDCARD="/sdcard/mlperf_resnet50"
    outputs="softmax_tensor:0"
    export SNPE_DATASET="imagenet_resnet50_snpe"
    model_type="classifier"
fi
if [ $name == "mobilenet-snpe" ] ; then
    model_path="$MODEL_DIR/mobilenet_v1_1.0_224.dlc"
    profile=mobilenet-snpe
    export PATH_SDCARD="/sdcard/mlperf_mobilenet"
    outputs="MobilenetV1/Predictions/Reshape_1:0"
    export SNPE_DATASET="imagenet_mobilenet_snpe"
    model_type="classifier"
fi
if [ $name == "ssd-mobilenet-snpe" ] ; then
    model_path="$MODEL_DIR/ssd_mobilenet_v1_coco_2018_01_28.dlc"
    profile=ssd-mobilenet-snpe
    export PATH_SDCARD="/sdcard/mlperf_ssd_mobilenet"
    outputs="num_detections:0,detection_boxes:0,detection_scores:0,detection_classes:0"
    export SNPE_DATASET="coco300_ssd_mobilenet_snpe"
    model_type="objectdetection"
fi
if [ $name == "ssd-resnet34-snpe" ] ; then
    model_path="$MODEL_DIR/ssd_resnet34_mAP_20.2.dlc"
    profile=ssd-resnet34-snpe
    export PATH_SDCARD="/sdcard/mlperf_ssd_resnet34"
    outputs="bboxes,labels,scores"
    export SNPE_DATASET="coco1200_resnet34_snpe"
    model_type="objectdetection"
fi

#
# tensorflow
#
if [ $name == "resnet50-tf" ] ; then
    model_path="$MODEL_DIR/resnet50_v1.pb"
    profile=resnet50-tf
fi
if [ $name == "mobilenet-tf" ] ; then
    model_path="$MODEL_DIR/mobilenet_v1_1.0_224_frozen.pb"
    profile=mobilenet-tf
fi
if [ $name == "ssd-mobilenet-tf" ] ; then
    model_path="$MODEL_DIR/ssd_mobilenet_v1_coco_2018_01_28.pb"
    profile=ssd-mobilenet-tf
fi
if [ $name == "ssd-resnet34-tf" ] ; then
    model_path="$MODEL_DIR/ssd_resnet34_mAP_20.2.pb"
    profile=ssd-resnet34-tf
fi

#
# onnxruntime
#
if [ $name == "resnet50-onnxruntime" ] ; then
    model_path="$MODEL_DIR/resnet50_v1.onnx"
    profile=resnet50-onnxruntime
fi
if [ $name == "mobilenet-onnxruntime" ] ; then
    model_path="$MODEL_DIR/mobilenet_v1_1.0_224.onnx"
    profile=mobilenet-onnxruntime
fi
if [ $name == "ssd-mobilenet-onnxruntime" ] ; then
    model_path="$MODEL_DIR/ssd_mobilenet_v1_coco_2018_01_28.onnx"
    profile=ssd-mobilenet-onnxruntime
fi
if [ $name == "ssd-resnet34-onnxruntime" ] ; then
    model_path="$MODEL_DIR/resnet34-ssd1200.onnx"
    profile=ssd-resnet34-onnxruntime
fi

#
# pytorch
#
if [ $name == "resnet50-pytorch" ] ; then
    model_path="$MODEL_DIR/resnet50_v1.onnx"
    profile=resnet50-onnxruntime
    extra_args="$extra_args --backend pytorch"
fi
if [ $name == "mobilenet-pytorch" ] ; then
    model_path="$MODEL_DIR/mobilenet_v1_1.0_224.onnx"
    profile=mobilenet-onnxruntime
    extra_args="$extra_args --backend pytorch"
fi
if [ $name == "ssd-resnet34-pytorch" ] ; then
    model_path="$MODEL_DIR/resnet34-ssd1200.pytorch"
    profile=ssd-resnet34-pytorch
fi


#
# tflite
#
if [ $name == "resnet50-tflite" ] ; then
    model_path="$MODEL_DIR/resnet50_v1.tflite"
    profile=resnet50-tf
    extra_args="$extra_args --backend tflite"
fi
if [ $name == "mobilenet-tflite" ] ; then
    model_path="$MODEL_DIR/mobilenet_v1_1.0_224.tflite"
    profile=mobilenet-tf
    extra_args="$extra_args --backend tflite"
fi

name="$name-$runtime"
EXTRA_OPS="$extra_args $EXTRA_OPS"
