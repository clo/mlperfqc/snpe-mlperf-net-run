#!/bin/bash

# Apply updates
if ls newpatch/updates/*.py >/dev/null 2>&1; then
   cd newpatch
   rm -rf a b patch
   mkdir -p a/jni/mlperf_snpe/snpe_python
   mkdir -p b/jni/mlperf_snpe/snpe_python
   mkdir patch
   cd ..

   cp external/mlperf_inference/v0.5/classification_and_detection/python/coco.py newpatch/a/jni/mlperf_snpe/snpe_python/coco_snpe.py
   cp external/mlperf_inference/v0.5/classification_and_detection/python/backend.py newpatch/a/jni/mlperf_snpe/snpe_python/backend_snpe.py
   cp external/mlperf_inference/v0.5/classification_and_detection/python/imagenet.py newpatch/a/jni/mlperf_snpe/snpe_python/imagenet_snpe.py
   cp external/mlperf_inference/v0.5/classification_and_detection/python/main.py newpatch/a/jni/mlperf_snpe/snpe_python/main_snpe.py

   cd newpatch
   for f in `ls updates/*.py`;
   do
      cp $f b/jni/mlperf_snpe/snpe_python/
   done

   # Regenerate patches
   for f in `ls b/jni/mlperf_snpe/snpe_python/`; do
      diff -Nu a/jni/mlperf_snpe/snpe_python/$f b/jni/mlperf_snpe/snpe_python/$f > patch/$f.patch
   done
   cd ..
fi
