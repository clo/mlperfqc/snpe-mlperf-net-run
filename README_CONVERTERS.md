# Model Conversion
Machine Learning frameworks have specific formats for storing neural network models. SNPE supports various models by converting them to a framework neutral deep learning container (DLC) format. The DLC file is used by the SNPE runtime for execution of the neural network. Mlperf supports following two frameworks:
* ## Tensorflow
The **snpe-tensorflow-to-dlc** tool converts a frozen TensorFlow model or a graph meta file into an equivalent SNPE DLC file.
* ## Onnx
The **snpe-onnx-to-dlc** tool converts a serialized ONNX model to an equivalent DLC representation.
## Prerequisites
1. Set SNPE_ROOT
2. Set Android_NDK_ROOT
3. Set Tensorflow_HOME_DIR
## *MobilenetV1*
### Tensorflow Model Conversion

```
snpe-tensorflow-to-dlc --graph mobilenet_v1_1.0_224_frozen.pb --input_dim input "1,224,224,3" --out_node MobilenetV1/Predictions/Reshape_1 --dlc mobilenet_v1_1.0_224.dlc --allow_unconsumed_nodes
```
### ONNX Model Conversion


```
snpe-onnx-to-dlc --input_network mobilenet_v1_1.0_224.onnx
                 --output_path mobilenet_v1_1.0_224.dlc
```
## *Resnet50*
### Tensorflow Model Conversion

```
snpe-tensorflow-to-dlc --graph resnet50_v1.pb --input_dim input "1,224,224,3" --out_node softmax_tensor --dlc resnet50_v1.5.dlc --allow_unconsumed_nodes
```
### ONNX Model Conversion


```
snpe-onnx-to-dlc --input_network resnet50_v1.onnx
                 --output_path resnet50_v1.5.dlc
 ```
 ## *SSD-Mobilenet*
### Tensorflow Model Conversion

```
snpe-tensorflow-to-dlc --graph ssd_mobilenet_v1_coco_2018_01_28/frozen_inference_graph.pb./snpe-tensorflow-to-dlc --input_dim Preprocessor/sub 1,300,300,3 - --out_node detection_classes --out_node detection_boxes --out_node detection_scores --dlc mobilenet_ssd.dlc --allow_unconsumed_nodes
```
### ONNX Model Conversion


```
snpe-onnx-to-dlc --input_network ssd-mobilenet.onnx
                 --output_path ssd-mobilenet.dlc
 ```
## More documentation on converters
https://developer.qualcomm.com/sites/default/files/docs/snpe/usergroup2.html